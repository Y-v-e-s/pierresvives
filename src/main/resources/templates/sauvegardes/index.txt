<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="https://www.thymeleaf.org"
      xmlns:sec="https://www.thymeleaf.org/thymeleaf-extras-springsecurity3">
<head>
    <title>Spring Security Example</title>
</head>
<body>
<h1>Welcome
    <block th:text="${who!=null && who.getUsername!=null} ? ${who.getUsername()} : ''"></block>
    !
</h1>

<p>Login : Click <a th:href="@{/login}">here</a></p>
<p>SEOSettings : Click <a th:href="@{/seosettings}">here</a></p>
<p>Registration : Click <a th:href="@{/registration}">here</a> to get a free and full access</p>
<p>Save <a th:href="@{/image}">the URLs</a> of the images</p>


<div th:text="${who!=null && who.getUsername!=null} ? ${useraccountdetails} : ''"></div>
<div th:text="${who!=null && who.getUsername!=null} ? ${who.getUsername()} : ''"></div>
<div th:text="${who!=null && who.getFirstname!=null} ? ${who.getFirstname()} : ''"></div>
<div th:text="${who!=null && who.getLastname!=null} ? ${who.getLastname()} : ''"></div>
<div th:text="${who!=null && who.getEmail!=null} ? ${who.getEmail()} : ''"></div>


<p>go to <a th:href="@{/seobot}"> SEO bot </a></p>

</body>
</html>