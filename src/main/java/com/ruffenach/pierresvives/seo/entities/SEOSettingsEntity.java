package com.ruffenach.pierresvives.seo.entities;

import com.ruffenach.pierresvives.seo.dataprocessing.production.StringToTable;
import org.springframework.data.annotation.Id;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
/**
 * Class representing SEO settings.
 * It is a pojo that benefits from a few additional methods including
 * methods for formatting attributes,
 * a toString() optimized for the web.
 */
public class SEOSettingsEntity {

    StringToTable stringToTable=new StringToTable();

    @Id
    private String id;

    private String username="";
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    public SEOSettingsEntity() {
    }

    private String ftphost = "FTP.host";
    private String ftplogin = "FTPUserName";
    private String ftppwd = "FTPUserPassword";
    private int ftpport = 21;
    private String ftppathtomaindir = "FTP.path.to.main.directory";

    private List<ImageEntity> imagelist = new ArrayList<>();

    private String pagetemplate = "";               //to be completed with the template page code
    private String pagelinkempty = "";              //to be completed with the link template page code
    private String linklist = "";                   //to populate pagetemplatelink and be populated page after page
    private String wordtouse = "";
    private String wordtoreplace = "";
    private String wordtoremove = "";
    private String synonym = "";
    private String pagelinkfull = "";

    private String[] wordstouse = new String[0];
    private String[] wordstoreplace = new String[0];
    private String[] wordstoremove = new String[0];
    private String[][] synonyms = new String[0][];
    
    private long threadid = -1;
    public long getThreadid() {
        return threadid;
    }
    public void setThreadid(long threadid) {
        this.threadid = threadid;
    }

    private boolean switchsoup = true;
    private String urlforbot = "";
    public boolean getSwitchsoup() {
        return switchsoup;
    }
    public void setSwitchsoup(boolean switchsoup) {
        this.switchsoup = switchsoup;
    }
    public String getUrlforbot() {
        return urlforbot;
    }
    public void setUrlforbot(String urlforbot) {
        this.urlforbot = urlforbot;
    }

    public SEOSettingsEntity(String username, String ftphost, String ftplogin, String ftppwd, int ftpport,
                             String ftppathtomaindir, List<ImageEntity> imagelist,
                             String pagetemplate, String pagelinkempty, String linklist,
                             String wordtouse, String wordtoreplace, String wordtoremove, String synonym,
                             String[] wordstouse, String[] wordstoreplace, String[] wordstoremove,
                             String[][] synonyms,
                             long threadid, boolean switchsoup, String urlforbot) {
        //this.id=userid;
        this.username = username;
        this.ftphost = ftphost;
        this.ftplogin = ftplogin;
        this.ftppwd = ftppwd;
        this.ftpport = ftpport;
        this.ftppathtomaindir = ftppathtomaindir;
        this.imagelist = imagelist;
        this.pagetemplate = pagetemplate;
        this.pagelinkempty = pagelinkempty;
        this.linklist = linklist;
        this.wordtouse = wordtouse;
        this.wordtoreplace = wordtoreplace;
        this.wordtoremove = wordtoremove;
        this.synonym = synonym;
        this.wordstouse = wordstouse;
        this.wordstoreplace = wordstoreplace;
        this.wordstoremove = wordstoremove;
        this.synonyms = synonyms;
        this.threadid = threadid;
        this.switchsoup = switchsoup;
        this.urlforbot = urlforbot;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getFtphost() {
        return ftphost;
    }
    public void setFtphost(String ftphost) {
        this.ftphost = ftphost;
    }

    public String getFtplogin() {
        return ftplogin;
    }
    public void setFtplogin(String ftplogin) {
        this.ftplogin = ftplogin;
    }

    public String getFtppwd() {
        return ftppwd;
    }
    public void setFtppwd(String ftppwd) {
        this.ftppwd = ftppwd;
    }

    public int getFtpport() {
        return ftpport;
    }
    public void setFtpport(int ftpport) {
        this.ftpport = ftpport;
    }

    public String getFtppathtomaindir() {
        return ftppathtomaindir;
    }
    public void setFtppathtomaindir(String ftppathtomaindir) {
        this.ftppathtomaindir = ftppathtomaindir;
    }

    public List<ImageEntity> getImagelist() {
        return imagelist;
    }
    public void setImagelist(List<ImageEntity> imagelist) {
        this.imagelist = imagelist;
    }

    public String getPagetemplate() {
        return pagetemplate;
    }
    public void setPagetemplate(String pagetemplate) {
        this.pagetemplate = pagetemplate;
    }

    public String getPagelinkempty() {
        return pagelinkempty;
    }
    public void setPagelinkempty(String pagelinkempty) {
        this.pagelinkempty = pagelinkempty;
    }

    public String getLinklist() {
        return linklist;
    }
    public void setLinklist(String linklist) {
        this.linklist = linklist;
    }

    public String getWordtouse() {
        return wordtouse;
    }
    public void setWordtouse(String wordtouse) {
        this.wordtouse = wordtouse;
        setWordstouse(aboutWordtouse(wordtouse));
    }

    public String getWordtoreplace() {
        return wordtoreplace;
    }
    public void setWordtoreplace(String wordtoreplace) {
        this.wordtoreplace = wordtoreplace;
        setWordstoreplace(aboutWordtoreplace(wordtoreplace));
    }

    public String getWordtoremove() {
        return wordtoremove;
    }
    public void setWordtoremove(String wordtoremove) {
        this.wordtoremove = wordtoremove;
        setWordstoremove(aboutWordtoremove(wordtoremove));
    }

    public String getSynonym() {
        return synonym;
    }
    public void setSynonym(String synonym) {
        this.synonym = synonym;
        setSynonyms(aboutSynonym(synonym));
    }

    public String getPagelinkfull() {
        return pagelinkfull;
    }
    public void setPagelinkfull(String pagelinkfull) {
        this.pagelinkfull = pagelinkfull;
    }

    public String[] getWordstouse() {
        return wordstouse;
    }
    public void setWordstouse(String[] wordstouse) {
        this.wordstouse = wordstouse;
    }

    public String[] getWordstoreplace() {
        return wordstoreplace;
    }
    public void setWordstoreplace(String[] wordstoreplace) {
        this.wordstoreplace = wordstoreplace;
    }

    public String[] getWordstoremove() {
        return wordstoremove;
    }
    public void setWordstoremove(String[] wordstoremove) {
        this.wordstoremove = wordstoremove;
    }

    public String[][] getSynonyms() {
        return synonyms;
    }
    public void setSynonyms(String[][] synonyms) {
        this.synonyms = synonyms;
    }

    @Override
    public String toString() {
        String s = "no image saved";
        StringBuilder sb = new StringBuilder();
        if (imagelist != null) {
            Iterator<ImageEntity> i = imagelist.iterator();
            while (i.hasNext()) {
                sb.append(i.next().toString());
                sb.append("<br>");
            }
            s = sb.toString();
        }
        return "SEO settings<br>" +
                "<br> id................. " + id +
                "<br> username........... " + username +
                "<br> threadid........... " + threadid +
                "<br> urlforbot.......... " + urlforbot +
                "<br> switchsoup......... " + switchsoup +
                "<br> ftphost............ " + ftphost +
                "<br> ftplogin........... " + ftplogin +
                "<br> ftppwd............. " + ftppwd +
                "<br> ftpport............ " + ftpport +
                "<br> ftppathtomaindir... " + ftppathtomaindir +
                "<br> imagelist.......... " + s +
                "<br><textarea disabled rows=\"18\" cols=\"36\"" +
                "style=\"background-color : #181818;" +
                "text-align : justify;" +
                "overflow-wrap : break-word;" +
                "font-family : Consolas;" +
                "color : lime;\"" +
                   ">pagetemplate........ " + pagetemplate +
                "</textarea><br>" +
                "<textarea disabled rows=\"18\" cols=\"36\"" +
                "style=\"background-color : #181818;" +
                "text-align : justify;" +
                "overflow-wrap : break-word;" +
                "font-family : Consolas;" +
                "color : lime;\"" +
                   ">pagelinkempty........ " + pagelinkempty +
                "</textarea>" +
                "<br> linklist............ " + linklist +
                "<br> wordtouse........... " + wordtouse +
                "<br> wordtoreplace....... " + wordtoreplace +
                "<br> wordtoremove........ " + wordtoremove +
                "<br> synonym............. " + synonym +
                "<br> [] wordstouse....... " + arrayToString(wordstouse) +
                "<br> [] wordstoreplace... " + arrayToString(wordstoreplace) +
                "<br> [] wordstoremove.... " + arrayToString(wordstoremove) +
                "<br> [][] synonyms....... " + arrayToString2D(synonyms) +
                "<br><br> ";
    }

    public String arrayToString(String[] s) {
        String z = "<br>....................";
        for (int i = 0; i < s.length; i++) {
            z = z + "<br>...................." + s[i];
        }
        z = z + "<br>";
        return z;
    }

    public String arrayToString2D(String[][] s) {
        String z = "<br>";
        for (int i = 0; i < s.length; i++) {
            for (int j = 0; j < s[0].length; j++) {
                z = z + "<br>...................." + s[i][j];
            }
            z = z + "<br>...........................";
        }
        z = z + "<br>";
        return z;
    }

    public String[] aboutWordtouse(String wordtouse) {
        String[] wu = new String[0];
        if (getWordtouse() != null && !getWordtouse().equals("")) {
            wu = stringToTable.stt_pipe(getWordtouse());
            //seoBotSettingsDTO.setWordstouse(wu);
        }
        return wu;
    }

    public String[] aboutWordtoreplace(String wordtoreplace) {
        String[] wrep = new String[0];
        if (getWordtoreplace() != null && !getWordtoreplace().equals("")) {
            wrep = stringToTable.stt_pipe(getWordtoreplace());
            //seoBotSettingsDTO.setWordstoreplace(wrep);
        }
        return wrep;
    }

    public String[] aboutWordtoremove(String wordtoremove) {
        String[] wrem = new String[0];
        if (getWordtoremove() != null && !getWordtoremove().equals("")) {
            wrem = stringToTable.stt_pipe(getWordtoremove());
            //seoBotSettingsDTO.setWordstoremove(wrem);
        }
        return wrem;
    }

    public String[][] aboutSynonym(String synonym) {
        String[][] table_synonym = new String[0][];
        if (getSynonym() != null && !getSynonym().equals("")) {//"synonym_chain.txt";
            String[] table = stringToTable.stt_pipe(synonym);
            table_synonym = new String[table.length][6];
            for (int i = 0; i < table.length; i++) {
                String[] t2 = stringToTable.stt_virgule(table[i]);
                if (t2.length < 2) {
                    table_synonym[i][0] = t2[0];
                    table_synonym[i][1] = t2[0];
                    continue;
                }
                System.arraycopy(
                        t2,
                        0,
                        table_synonym[i],
                        0,
                        t2.length);
            }
            //System.out.println(Arrays.deepToString(table_synonym));
            //seoBotSettingsDTO.setSynonyms(table_synonym);
        }
        return table_synonym;
    }
}