package com.ruffenach.pierresvives.seo.entities;

import org.springframework.data.annotation.Id;
/**
 * Class representing an image.
 * It is a pojo that benefits from a toString() optimized for the web.
 */
public class ImageEntity {

    @Id
    private String id;

    private String imgname = "";
    private String imgaddress = "";

    private String username="";
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    //    private String userid = "";           //get user id, if end session & userid empty then delete all db & dto
//    public String getUserid() {
//        return userid;
//    }
//    public void setUserid(String userid) {
//        this.userid = userid;
//    }
//    public ImageEntity(String imgname, String imgaddress, String userid) {
//        this.imgname = imgname;
//        this.imgaddress = imgaddress;
//        this.userid = userid;
//    }

    public ImageEntity() {
    }
    public ImageEntity(String imgname, String imgaddress, String username) {
        this.imgname = imgname;
        this.imgaddress = imgaddress;
        this.username=username;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getImgname() {
        return imgname;
    }
    public void setImgname(String imgname) {
        this.imgname = imgname;
    }

    public String getImgaddress() {
        return imgaddress;
    }
    public void setImgaddress(String imgaddress) {
        this.imgaddress = imgaddress;
    }

    @Override
    public String toString() {
         return "<br> imgname...... " + imgname +
                 "<br> imgaddress... " + imgaddress +
                 "<br> <img src='" + imgaddress + "'" +
                 " alt='" + imgname + "'" +
                 " height='240'>" +
                 " <br> ";
    }
}