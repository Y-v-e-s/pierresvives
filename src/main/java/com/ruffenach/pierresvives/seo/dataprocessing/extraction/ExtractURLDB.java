package com.ruffenach.pierresvives.seo.dataprocessing.extraction;

import com.ruffenach.pierresvives.seo.website.repositories.UrlDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
/**
 * Class to extract URLs from a web page and save them
 */
@Service
public class ExtractURLDB {

    @Autowired
    UrlDAO urlDAO;

    private boolean stopthreading = true;
    public void setNotStopThread(boolean b) { this.stopthreading = b; }

    public void extractURL(String webPage, String username) {
        StringBuilder str = new StringBuilder();    //String str = "";
        boolean b = false;
        boolean bb = false;
        for (int i = 0; i < webPage.length(); i++) {

            if(!stopthreading) break;

            if (webPage.charAt(i) == 'h') {
                if (webPage.charAt(i + 1) == 'r') {
                    if (webPage.charAt(i + 2) == 'e') {
                        if (webPage.charAt(i + 3) == 'f') {
                            b = true;
                        }
                    }
                }
            }
            if (b == true && webPage.charAt(i) == '"') {
                bb = true;
                b = false;
                continue;
            }
            if (b == false && webPage.charAt(i) == '"' && str.length() > 0) {
                bb = false;
                if (str.toString().toLowerCase().charAt(0) == 'h') {
                    urlDAO.create(str.toString(), username); //à filtrer
                }
                str.setLength(0);//new StringBuilder();
            }
            if (bb == true) {
                str.append(webPage.charAt(i));  //str = str + webPage.charAt(i);
            }
        }
    }
}
