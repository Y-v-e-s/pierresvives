package com.ruffenach.pierresvives.seo.dataprocessing.extraction;

import org.springframework.stereotype.Service;
/**
 * Class to extract the description of a web page
 */
@Service
public class ExtractDescription {

    private boolean stopthreading = true;
    public void setNotStopThread(boolean b) {
        this.stopthreading = b;
    }

    public String extractDescription(String webPage) {
        String[] vars = {"description", "content", "\"", "\""};
        StringBuilder str = new StringBuilder();    //String str = "";
        int j = 0;

        for (int i = 0; i < webPage.length(); i++) {

            if(!stopthreading) return"";

            if (j == 3) {
                str.append(webPage.charAt(i));  //str = str + webPage.charAt(i);
            }

            StringBuilder s = new StringBuilder();  //String s = "";
            for (int ii = 0; ii < vars[j].length(); ii++) {

                if ((i + ii) >= webPage.length()) {
                    break;
                }

                if (webPage.charAt(i + ii) == vars[j].charAt(ii)) {
                    s.append(webPage.charAt(i + ii)); //s = s + webPage.charAt(i + ii);
                    if (s.toString().equals(vars[j])) {
                        j++;
                        i = i + ii;
                        break;
                    }
                }
            }
            if (j == 4) {
                break;
            }
        }
        if (str.length() > 12) {
            webPage = str.substring(0, str.length() - 1);
        } else {
            webPage = "";
        }
        return webPage;
    }
}