package com.ruffenach.pierresvives.seo.dataprocessing.production;


import com.ruffenach.pierresvives.seo.dataprocessing.extraction.*;
import com.ruffenach.pierresvives.seo.dataprocessing.navigation.URLRequest;
import com.ruffenach.pierresvives.seo.website.entities.ResourceEntity;
import com.ruffenach.pierresvives.seo.website.repositories.ResourceDAO;
import com.ruffenach.pierresvives.seo.website.repositories.UrlDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;

/**
 * Resource search and download class.
 * It is a Spring component whose scope is the prototype.
 *
 * A bean with prototype scope will return a different instance each time the container is requested.
 * The container creates an instance of this class for each new use and therefore also for each user.
 * Also note that each new instance is born in a separate thread (in SEOBotController.class).
 */
@Service
@Scope("prototype")
public class Harvester {

    @Autowired
    UrlDAO urlDAO;

    @Autowired
    ExtractURLDB extractURLDB;

    @Autowired
    ExtractTextJsoup extractTextJsoup;

    @Autowired
    ExtractTitle extractTitle;

    @Autowired
    ExtractDescription extractDescription;

    @Autowired
    ResourceDAO resourceDAO;

    private String username;

    URLRequest urlRequest = new URLRequest();

    private boolean notStopCurrentThread = true;
    public void setNotStopThread(boolean b) {
        this.notStopCurrentThread = b;
        extractURLDB.setNotStopThread(b);
        extractTitle.setNotStopThread(b);
        extractDescription.setNotStopThread(b);
    }

    ResourceEntity resourceModel;

    private StringBuilder pageDescription;
    private String pageUrl;

    private String userId = "";

    public void setUserId(String userId) {
        this.userId = userId;
    }

    private String url = "";

    public void setUrl(String url) {
        this.url = url;
    }

    private boolean switchSoup = true;

    public void setSwitchSoup(boolean switchSoup) {
        this.switchSoup = switchSoup;
    }

    private String[] wordstouse = new String[0];
    private String[][] synonyms = new String[0][];

    public void setWordstouse(String[] wordstouse) {
        this.wordstouse = wordstouse;
    }

    public void setSynonyms(String[][] synonyms) {
        this.synonyms = synonyms;
    }

    public Harvester() {}

    public ResourceEntity harvesting(String usernm) {
        this.username=usernm;
        resourceModel = new ResourceEntity();
        resourceModel.setUsername(username);
        System.out.println("username : " + username);
        //resourceDTO.init();
        //resourceDTO.setUserid(userDTO.getId());
        resourceModel.setUserid(userId);//resourceModel.setUserid(userDTO.getId());

        String text = "";
        do {
            System.out.println("url : " + url);
            text = urlRequest.urlRequest(url);
            System.out.println("notStopCurrentThread : "+notStopCurrentThread);
            if (notStopCurrentThread) {
                url = urlDAO.getUrlAndDelete(username);//seoBotSettingsDTO.setUrlforbot(urlDAO.getUrlAndDelete());
            }
            //if (text.equals("stopharvesting")) return true;
        }
        while (notStopCurrentThread && text.equals("") && urlDAO.getSize(username) > 0);
        if (notStopCurrentThread && !text.equals("")) extractURLDB.extractURL(text, username);
        if (notStopCurrentThread && !text.equals("")) aboutTitle(text);
        //if (!text.equals("") && seoSettingsDTO.getSynonyms() != null && notStopCurrentThread) aboutDescription(text);
        if (notStopCurrentThread && !text.equals("") && synonyms != null) aboutDescription(text);
        if (notStopCurrentThread && pageDescription != null) aboutPageUrl();
        if (notStopCurrentThread && !text.equals("")) aboutText(text);
        return resourceModel;
    }

    public void aboutText(String text) {
        //extraction
        if (switchSoup) {//(seoBotSettingsDTO.getSwitchsoup()) {
            text = extractTextJsoup.extractTextJsoup(text);
        } else {
/*
            text = extractText.extractText(text);
            text = extractTextJv.disseq(text);

            if (pageDescription!=null && text!=null && !text.equals("")) {text = pageDescription.toString();}
            for (int i = text.length() - 1; i >= 0; i--) {
                text = text.replace("  ", " ");
            }
*/
        }
        //resourceDTO.setRtext(text);
        resourceModel.setRtext(text);
        //if (userId != null && !userId.equals("")) {//if (userDTO.getUser() != null && !userDTO.getUser().equals("")) {
            //resourceModel.copyDTOToModel(resourceDTO);
            resourceDAO.create(resourceModel);
        //}
    }

    public void aboutPageUrl() {
        //construction...
        int hlf = pageDescription.length() / 2;
        pageUrl = pageDescription.substring(0, hlf);
        pageUrl = pageUrl.trim();
//pageUrl = pageUrl.replace(" ", "_");
        pageUrl = pageUrl.replace("é", "e");
        pageUrl = pageUrl.replace("è", "e");
        pageUrl = pageUrl.replace("ê", "e");
        pageUrl = pageUrl.replace("ü", "u");
        pageUrl = pageUrl.replace("û", "u");
        pageUrl = pageUrl.replace("à", "a");
        pageUrl = pageUrl.replace("ç", "c");
        pageUrl = pageUrl.replace("ù", "u");
        pageUrl = pageUrl.replace("%", " ");
        pageUrl = pageUrl.replace("\\", " ");
        pageUrl = pageUrl.replace("/", " ");
        pageUrl = pageUrl.replaceAll("[^\\x20-\\x7e]", " ");

        //String fixed = original.replaceAll("[^\\u0000-\\uFFFF]", "");
        //ByteBuffer bb = StandardCharsets.UTF_8.encode(pageUrl);
        //pageUrl = StandardCharsets.UTF_8.decode(bb).toString();

        byte[] byt = pageUrl.getBytes(StandardCharsets.UTF_8);
        pageUrl = new String(byt, StandardCharsets.UTF_8);

        System.out.println("pageUrl : " + pageUrl);
        //resourceDTO.setRpageurl(pageUrl);
        resourceModel.setRpageurl(pageUrl);
        //if (userId != null && !userId.equals("")) {//if (userDTO.getUser() != null && !userDTO.getUser().equals("")) {
            //resourceModel.copyDTOToModel(resourceDTO);
            resourceDAO.create(resourceModel);
        //}
    }

    public void aboutDescription(String text) {
        String page_description = extractDescription.extractDescription(text);


/*
        //extraction
        //pageDescription = new StringBuilder(extractDescription.extractDescription(text));
        //traitement :
        à placer dans une méthode optionnelle
        String page_description = "";

        Random r = new Random();

        if (pageDescription.length() < 12 ||                    //pageDescription == null ||
                pageDescription.toString().contains(">") ||
                pageDescription.toString().contains("<") ||
                pageDescription.toString().contains("=")) {
            pageDescription = new StringBuilder();
            int n = r.nextInt((12 - 3) + 1) + 3;
            for (int i = 1; i < n + 1; i++) {
                if (wordstouse != null) {
                    int nb = r.nextInt((wordstouse.length - 1) + 1);
                    pageDescription.append(" ").append(wordstouse[nb]);
                }
            }
            page_description = pageDescription.toString();
//System.out.println("page_description : "+page_description);
        } else {
            int l;
            for (String[] strings : synonyms) {
                if (strings[0] == null || strings[1] == null) continue;
                do {
                    l = r.nextInt(strings.length - 1) + 1;
                } while (strings[l] == null);
                page_description = pageDescription.toString();
                page_description =
                        page_description.replace((strings[0] + " "), (strings[l] + " "));
                page_description =
                        page_description.replace((strings[0] + "s "), (strings[l] + "s "));
            }
        }
*/


        //resourceDTO.setRdescription(page_description);
        resourceModel.setRdescription(page_description);
        //if (userId != null && !userId.equals("")) {//if (userDTO.getUser() != null && !userDTO.getUser().equals("")) {
            //resourceModel.copyDTOToModel(resourceDTO);
        resourceDAO.create(resourceModel);
        //}
    }

    public void aboutTitle(String text) {
        String pageTitle = extractTitle.extractTitle(text);

/*
        //traitement :
        à mettre en option (case à cocher ou curseur, etc...)
        for (int i = pageTitle.length() - 1; i >= 0; i--) {
            pageTitle = pageTitle.replace("  ", " ");
        }
        if (pageTitle.length() < 3 ||
                pageTitle.contains(">") ||
                pageTitle.contains("<")
        ) {
            pageTitle += " " + System.currentTimeMillis();//à remplacer par une chaine aléatoire bcp + pertinente
        }
*/

        //resourceDTO.setRtitle(pageTitle);
        //resourceDTO.setUserid(userDTO.getId());
        resourceModel.setRtitle(pageTitle);
        //if (userId != null && !userId.equals("")) {//if (userDTO.getUser() != null && !userDTO.getUser().equals("")) {
            //resourceModel.copyDTOToModel(resourceDTO);
            //resourceDTO.setId(resourceDAO.create(resourceModel));
            resourceModel.setId(resourceDAO.create(resourceModel));
        //}
    }
}


//    public void aboutSynonym() {
//        synonym = seoSettingsDTO.getSynonym();   //"synonym_chain.txt";
//        String[] table = stringToTable.stt_pipe(synonym);
//        table_synonym = new String[table.length][6];
//        for (int i = 0; i < table.length; i++) {
//            String[] t2 = stringToTable.stt_virgule(table[i]);
//            if (t2.length < 2) {
//                table_synonym[i][0] = t2[0];
//                table_synonym[i][1] = t2[0];
//                continue;
//            }
//            System.arraycopy(
//                    t2,
//                    0,
//                    table_synonym[i],
//                    0,
//                    t2.length);
//        }
//        //System.out.println(Arrays.deepToString(table_synonym));
//    }
//}
