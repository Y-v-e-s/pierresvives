package com.ruffenach.pierresvives.seo.dataprocessing.extraction;

import com.ruffenach.pierresvives.seo.ftp.FTPManager;
import org.springframework.stereotype.Service;
/**
 * Class to extract URLs from a web page and save them via FTP on the user's site (replacing the local db).
 * @deprecated
 */
public class ExtractURLFTP {

    void extractURL(
            String user,
            String password,
            String host,
            String port,
            String ftpPath,
            String directory_urls,
            String webPage) {

        FTPManager ftpClient = new FTPManager();

        StringBuilder str = new StringBuilder();    //String str = "";
        boolean b = false, bb = false;

        for (int i = 0; i < webPage.length(); i++) {
            if (webPage.charAt(i) == 'h') {
                if (webPage.charAt(i + 1) == 'r') {
                    if (webPage.charAt(i + 2) == 'e') {
                        if (webPage.charAt(i + 3) == 'f') {
                            b = true;
                        }
                    }
                }
            }
            if (b == true && webPage.charAt(i) == '"') {
                bb = true;
                b = false;
                continue;
            }
            if (b == false && webPage.charAt(i) == '"' && str.length() > 0) {
                bb = false;
                if (str.toString().toLowerCase().charAt(0) == 'h') {
                    String s = ""+System.nanoTime();
                    String ftp_url_path = ftpPath + directory_urls + s;
                    ftpClient.ftpUploadMemoryText(user, password, str.toString(), host, port, ftp_url_path);
                }
                str = new StringBuilder();   //str = "";
            }
            if (bb == true) {
                str.append(webPage.charAt(i));  //str = str + webPage.charAt(i);
            }
        }
    }
}
