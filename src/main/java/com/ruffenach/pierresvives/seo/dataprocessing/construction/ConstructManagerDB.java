package com.ruffenach.pierresvives.seo.dataprocessing.construction;

import com.ruffenach.pierresvives.dto.SEOSettingsSessionService;
import com.ruffenach.pierresvives.dto.UserSessionService;
import com.ruffenach.pierresvives.dto.WebPageDTO;
import com.ruffenach.pierresvives.registration.repositories.RegistrationService;
import com.ruffenach.pierresvives.seo.dataprocessing.extraction.ExtractVAR;
import com.ruffenach.pierresvives.seo.entities.ImageEntity;
import com.ruffenach.pierresvives.seo.entities.SEOSettingsEntity;
import com.ruffenach.pierresvives.seo.repositories.ImagePersistentService;
import com.ruffenach.pierresvives.seo.repositories.SEOSettingsPersistentService;
import com.ruffenach.pierresvives.seo.website.entities.ResourceEntity;
import com.ruffenach.pierresvives.seo.website.entities.WebPageEntity;
import com.ruffenach.pierresvives.seo.website.repositories.ResourceDAO;
import com.ruffenach.pierresvives.seo.website.repositories.WebPageDAO;
import com.ruffenach.pierresvives.user.entities.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.Random;

/**
 * Class to build new pages from saved data.
 * It is a Spring component whose scope is the prototype.
 *
 * A bean with prototype scope will return a different instance each time the container is requested.
 * The container creates an instance of this class for each new use and therefore also for each user.
 * Also note that each new instance is born in a separate thread (in SEOBotController.class).
 */
@Service
@Scope("prototype")
public class ConstructManagerDB {

    @Autowired
    UserSessionService userDTO;

    @Autowired
    ImagePersistentService imageDAO;

    @Autowired
    SEOSettingsSessionService seoSettingsDTO;

    @Autowired
    SEOSettingsPersistentService seoSettingsDAO;

    @Autowired
    WebPageDAO webPageDAO;

    @Autowired
    ExtractVAR extractVAR;

    @Autowired
    ResourceDAO resourceDAO;

    @Autowired
    WebPageDTO webPageDTO;

    @Autowired
    RegistrationService registrationService;

    Random r = new Random();
    public Random getR() {
        return r;
    }
    public void setR(Random r) {
        this.r = r;
    }

    private String username;
    private WebPageEntity webPageEntity;
    private ResourceEntity resourceModel;
    private SEOSettingsEntity seoSettingsEntity;

    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public WebPageEntity getWebPageEntity() {
        return webPageEntity;
    }
    public void setWebPageEntity(WebPageEntity webPageEntity) {
        this.webPageEntity = webPageEntity;
    }
    public ResourceEntity getResourceModel() {
        return resourceModel;
    }
    public void setResourceModel(ResourceEntity resourceModel) {
        this.resourceModel = resourceModel;
    }
    public SEOSettingsEntity getSeoSettingsEntity() {
        return seoSettingsEntity;
    }
    public void setSeoSettingsEntity(SEOSettingsEntity seoSettingsEntity) {
        this.seoSettingsEntity = seoSettingsEntity;
    }

//    private String usernom;
//    public String getUsernom() {
//        return usernom;
//    }
//    public void setUsernom(String usernom) {
//        this.usernom = usernom;
//    }

//    static final String URL_LIST = "url.list.txt"; //peut intégrer les settings
//    static final String PAGE_LINK_TEMPLATE = "page.link.template";  //peut intégrer les settings
//    static final String PAGE_LINK = "link.html";  //peut intégrer les settings (et surtout le contenu)

    private String[] wu;
    private String[] words_replace;
    private String[][] table_synonym;
    private String page_template;

    private String pageDescription;
    private String pageTitle;
    private String text;
    private String pageUrl;

    public String[] getWu() {
        return wu;
    }
    public void setWu(String[] wu) {
        this.wu = wu;
    }
    public String[] getWords_replace() {
        return words_replace;
    }
    public void setWords_replace(String[] words_replace) {
        this.words_replace = words_replace;
    }
    public String[][] getTable_synonym() {
        return table_synonym;
    }
    public void setTable_synonym(String[][] table_synonym) {
        this.table_synonym = table_synonym;
    }
    public String getPage_template() {
        return page_template;
    }
    public void setPage_template(String page_template) {
        this.page_template = page_template;
    }
    public String getPageDescription() {
        return pageDescription;
    }
    public void setPageDescription(String pageDescription) {
        this.pageDescription = pageDescription;
    }
    public String getPageTitle() {
        return pageTitle;
    }
    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }
    public String getText() {
        return text;
    }
    public void setText(String text) {
        this.text = text;
    }
    public String getPageUrl() {
        return pageUrl;
    }
    public void setPageUrl(String pageUrl) {
        this.pageUrl = pageUrl;
    }

    public ConstructManagerDB(){}

    public void builder(String usernm) {
        this.username=usernm;
        System.out.println("ConstructManagerDB : username " + username);

        seoSettingsEntity=new SEOSettingsEntity();

        UserEntity userEntity=registrationService.getByUsername(username);
        seoSettingsEntity= seoSettingsDAO.readById(userEntity.getId());
//        seoSettingsEntity= seoSettingsDAO.readByUsername(username);
//        seoSettingsDTO.entityToService(seoSettingsEntity);

        webPageEntity=new WebPageEntity();
        resourceModel = resourceDAO.getResourceAndDelete(usernm);
        
        wu = seoSettingsEntity.getWordstouse();
        words_replace = seoSettingsEntity.getWordstoreplace();
        table_synonym = seoSettingsEntity.getSynonyms();
        page_template = seoSettingsEntity.getPagetemplate();

        pageDescription = resourceModel.getRdescription();
        pageTitle = resourceModel.getRtitle();
        text = resourceModel.getRtext();
        pageUrl = resourceModel.getRpageurl();

/*
//différentes opérations à tester ultérieurement et progressivement
        text = replaceSynonym(text, table_synonym); //remplacer certains mots par des synonymes
        text = replaceNumber(text);
        text = replaceWord(text, words_replace);
*/

        if(getAllImageAddress()!=null && getAllImageAddress().length>0) page_template = replaceVar(page_template);
        //remplacer var par adresse image
        page_template = extractVAR.extractVAR("vartitle", pageTitle, page_template);
        System.out.println("ConstructManagerDB : page_template vartitle " + page_template);
        page_template = extractVAR.extractVAR("vardescription", pageDescription, page_template);
        System.out.println("ConstructManagerDB : page_template vardescription " + page_template);
        page_template = extractVAR.extractVAR("vartext", text, page_template);

//        webPageDTO.setUrl(pageUrl);
        webPageEntity.setUrl(pageUrl); //changement de stratégie : de sessionscope à new

        if(wu!=null && wu.length>0) pageUrl = modifUrl(pageUrl, wu);
        String urlList = URLManager(pageTitle, pageUrl);
        page_template = page_template.replace("varlink", urlList);

        //if (!pageUrl.endsWith(".html")) pageUrl = pageUrl + ".html";

//        webPageDTO.setUserid(seoSettingsEntity.getId()); //userid = id settings
//        webPageDTO.setCodesource(page_template);
        webPageEntity.setUsername(usernm); //changement de stratégie : de sessionscope à new et de userid à username
        webPageEntity.setCodesource(page_template);

        webPageDAO.create(webPageEntity);
        //if (userDTO.getUser() != null && !userDTO.getUser().equals("")) {
            //pour sauvegarder la page et la liste de liens page link et link list
            seoSettingsDAO.create(seoSettingsEntity);
//            seoSettingsDAO.createWithId(
//                    seoSettingsDTO.getId(),
//                    seoSettingsDTO.getLinklist(),
//                    seoSettingsDTO.getPagelinkfull());
        //}
//        webPageDTO.init(); //réinitiliser le dto
    }

    public String modifUrl(String pageUrl, String[] wu) {
        //modif url page
        if (pageUrl.length() < 6) {                             //if (pageUrl.equals("") || pageUrl.length()<6) {
            int n = r.nextInt((6 - 3) + 1) + 3;
            StringBuilder pageUrlBuilder = new StringBuilder(pageUrl);
            for (int i = 1; i < n + 1; i++) {
                int nb = r.nextInt((wu.length - 1) + 1);
                pageUrlBuilder.append(" ").append(wu[nb]);      //pageUrl = pageUrl + "_" + wu[nb];
            }
            pageUrl = pageUrlBuilder.toString();
            pageUrl = pageUrl.substring(1);
        }
        pageUrl = pageUrl.replace("&", " ");
        pageUrl = pageUrl.replace(";", " ");
        pageUrl = pageUrl.replace(".", " ");
        pageUrl = pageUrl.replace(",", " ");

        pageUrl = System.currentTimeMillis() + pageUrl;
        if (pageUrl.length() > 1960) pageUrl = pageUrl.substring(0, 1959); // ?
        pageUrl = pageUrl + ".html";
        return pageUrl;
    }


    public String replaceNumber(String text) {
        for (int i = 2; i < 9; i++) {
            int j = r.nextInt(8) + 2;
            text = text.replace("" + i, "" + j);
//System.out.println("replace number : "+i+" "+j);
        }
        return text;
    }

    public String replaceWord(String text, String[] words_replace) {
        for (int i = 0; i < (words_replace.length - 1); i = i + 2) {
            text = text.toLowerCase().replace(words_replace[i], words_replace[i + 1]);
//System.out.println("words_replace[i],words_replace[i+1] : "+words_replace[i]+" "+words_replace[i+1]);
        }
        return text;
    }


    public String replaceSynonym(String text, String[][] table_synonym) {
        int z;
        //opération initiale pour ne pas altérer l'adresse de l'image ou autres éléments nouveaux
        //ne traite que le text et pas le page_template
        for (String[] strings : table_synonym) {
            if (strings[0] == null || strings[1] == null) continue;
            do {
                z = r.nextInt(strings.length - 1) + 1;
            } while (strings[z] == null);
            text = text.replace((strings[0] + " "), (strings[z] + " "));
            text = text.replace((strings[0] + "s "), (strings[z] + "s "));
        }
        return text;
    }

    public String replaceVar(String page_template) {
        //version une image, sans boucle = une seule image est utilisée, choisie au hazard
        return extractVAR.extractVAR(
                "varimage",
                getAllImageAddress()[r.nextInt(getAllImageAddress().length)],
                page_template);
    }

    /*
    méthode pour mettre à jour une page de liens à partir d'une liste de liens existants
    et sauvegarder la liste des liens à jour
        // récupérer la page des liens vierg = page html à publier avec tous les liens (mise à jour)
        // PageLink contient varlink
        // intégrer le nouveau lien dans la page des liens
        // en remplaçant la variable varlink de la page de liens par les liens
        // sauvegarder la page des liens
     */
    public String URLManager(String pageTitle, String pageUrl) {

        String urlList = seoSettingsEntity.getLinklist();

        urlList = "<ul><a href=\"" + pageUrl + "\">" + pageTitle + "</a></ul>" + urlList;
        //seoSettingsDTO.setLinklist(urlList);
        seoSettingsEntity.setLinklist(urlList);

        String page_link_content = seoSettingsEntity.getPagelinkempty();

        page_link_content = page_link_content.replace("varlink", urlList);
        seoSettingsEntity.setPagelinkfull(page_link_content);
        seoSettingsEntity.setPagelinkfull(page_link_content);
        return urlList;
    }

    /**
     * Returns an array of SEO settings image addresses.
     * This method uses a getter of the SEO settings entity.
     * @return an array of SEO settings image addresses
     */
    public String[] getAllImageAddress() {
        String[] s = new String[seoSettingsEntity.getImagelist().size()];
        int i = 0;
        Iterator<ImageEntity> iterator = seoSettingsEntity.getImagelist().iterator();
        while (iterator.hasNext()) {
            s[i] = iterator.next().getImgaddress();
            System.out.println("ConstructManagerDB : images adresses : " + s[i]);
            i++;
        }
        return s;
    }
}
