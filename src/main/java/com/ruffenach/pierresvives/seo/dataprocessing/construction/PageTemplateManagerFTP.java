package com.ruffenach.pierresvives.seo.dataprocessing.construction;

import com.ruffenach.pierresvives.seo.dataprocessing.extraction.ExtractVAR;
import com.ruffenach.pierresvives.seo.dataprocessing.navigation.URLRequest;
import com.ruffenach.pierresvives.seo.ftp.FTPManager;
/**
 * Add-on class to build new pages without local database but using client site for data backup via FTP
 * @deprecated
 */
public class PageTemplateManagerFTP {

    URLRequest WPQ = new URLRequest();
    ExtractVAR xtr = new ExtractVAR();
    FTPManager ftpClient = new FTPManager();

    String page_template(
            String[] img,
            String page_template,
            String user,
            String password,
            String host,
            String port,
            String ftpPath,
            String directory_preferences,
            String directory_images) {

//        PrintWriter out = response.getWriter();

//        out.println("<br> ......... you can either use template page provided by us "
//        		+ "or use yours<br>");
        if (page_template.equals("")) {
//	out.println("<br> ......... there it turns out that we provide the template<br>");

            page_template = WPQ
                    .urlRequest(null);//"https://1001alsaces.com/seobot/page_template.html");


            for (int i = 0; i < img.length; i++) {
                if (!img[i].equals("")) {
                    String new_http_path_img = directory_images + img[i];
                    //image already in images directory
                    String varimg = "varimg" + i;
                    page_template = xtr.extractVAR(varimg, new_http_path_img, page_template);
                    //the image name needs to be in the page_template
                }
            }

        }

        String path_pref_page_template = ftpPath + directory_preferences + "page_template.html";
        ftpClient
                .ftpUploadMemoryText(user, password, page_template, host, port, path_pref_page_template);

        return page_template;
    }
}

