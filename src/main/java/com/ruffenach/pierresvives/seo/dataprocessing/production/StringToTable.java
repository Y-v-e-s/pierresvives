package com.ruffenach.pierresvives.seo.dataprocessing.production;

import org.springframework.stereotype.Service;
/**
 * Class to transform a String into an array according to the separators (comma or pipe).
 */
@Service
public class StringToTable {

    public String[] stt_virgule(String s) {    //String z,
        String[] tab;
        for (int i = 0; i < s.length(); i++) {
            s = s.replace(",,", ",");   //zz,z
        }
        if (s.charAt(s.length() - 1) == ',') s = s.substring(0, s.length() - 1);    //z
        if (s.charAt(0) != ',') s = "," + s;    // z z
        if (s == null || s.equals("")) s = ",seobot";   //zseobot
        int j = 0;
        String c1 = "";
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == ',') {   //z
                j++;
            }
        }
        tab = new String[j];
        int jz = -1;
        int iz;
        for (iz = 0; iz < s.length(); iz++) {
            if (s.charAt(iz) == ',') {  //z
                if (c1.equals("")) {
                    jz++;
                    continue;
                }
                tab[jz] = c1;
                c1 = "";
                jz++;
                continue;
            }
            c1 = c1 + s.charAt(iz);
        }
        tab[jz] = c1;
        for (int i = 0; i < tab.length; i++) {
            if (tab[i] == null) tab[i] = "seobot";
            if (tab[i].equals("")) tab[i] = "seobot";
        }
        return tab;
    }

    public String[] stt_pipe(String s) {
        String[] tab;
        for (int i = 0; i < s.length(); i++) {
            s = s.replace("||", "|");
        }
        if (s.charAt(s.length() - 1) == '|') s = s.substring(0, s.length() - 1);
        if (s.charAt(0) != '|') s = "|" + s;
        if (s == null || s.equals("")) s = "|SEObot";
        int j = 0;
        StringBuilder c1 = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '|') {
                j++;
            }
        }
        tab = new String[j];
        int jz = -1;
        int iz;
        for (iz = 0; iz < s.length(); iz++) {
            if (s.charAt(iz) == '|') {
                if (c1.toString().equals("")) {
                    jz++;
                    continue;
                }
                tab[jz] = c1.toString();
                c1 = new StringBuilder();
//System.out.println("stringToTable "+tab[jz]);
                jz++;
                continue;
            }
            c1.append(s.charAt(iz));
        }
        tab[jz] = c1.toString();
        for (int i = 0; i < tab.length; i++) {
            if (tab[i] == null) tab[i] = "seobot";
            if (tab[i].equals("")) tab[i] = "seobot";
        }
        return tab;
    }
}