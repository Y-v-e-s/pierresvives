package com.ruffenach.pierresvives.seo.dataprocessing.extraction;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;
/**
 * Class to extract texts from a web page with Jsoup
 */
@Service
public class ExtractTextJsoup {

    private boolean stopthreading = true;
    public void setStopthreading(boolean b) {
        this.stopthreading = b;
    }

    public String extractTextJsoup(String html_page) {  //String extract(String html_page) {
        Document doc = Jsoup.parse(html_page);
        html_page = doc.select("blockquote").text()
                + doc.select("cite").text()
                + doc.select("q").text()
                + doc.select("strong").text()
                + doc.select("em").text()
                + doc.select("mark").text()
                + doc.select("h1").text()
                + doc.select("h2").text()
                + doc.select("h3").text()
                + doc.select("h4").text()
                + doc.select("h5").text()
                + doc.select("h6").text()
                + doc.select("p").text()
                + doc.select("del").text()
                + doc.select("dfn").text()
                + doc.select("dt").text()
                + doc.select("dd").text()
                + doc.select("caption").text()
                + doc.select("legend").text()
                + doc.select("label").text()
                + doc.select("article").text()
                + doc.select("aside").text();
//                + doc.select("pre").text()
//                + doc.select("code").text();
        return html_page;
    }
}