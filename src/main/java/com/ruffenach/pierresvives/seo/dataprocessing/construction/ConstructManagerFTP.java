package com.ruffenach.pierresvives.seo.dataprocessing.construction;

import com.ruffenach.pierresvives.seo.dataprocessing.extraction.ExtractVAR;
import com.ruffenach.pierresvives.seo.ftp.FTPConnection;
import com.ruffenach.pierresvives.seo.ftp.FTPManager;
import java.util.Random;

/**
 * Class to build new pages without local database but using client site for data backup via FTP
 * @deprecated
 */
public class ConstructManagerFTP {
    
    FTPConnection ftpx = new FTPConnection();
    //FTPManager ftpClient = new FTPManager();
    ExtractVAR xtr = new ExtractVAR();

    String urlList;
    Random r;

    public void reconstruction(
            String[] img_names,
            int nb_images,
            String[] words_replace,
            String[][] table_synonym,
            int iport,
            String[] wu,
            String page_template,
            String page_link,
            String directory_preferences,
            String directory_images,
            String pageUrl,
            String pageDescription,
            String pageTitle,
            String text,
            String host,
            String user,
            String password,
            String port,
            String ftpPath) {

        try {
            r = new Random();
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("pageUrl ******************************************************** "+pageUrl);

        boolean bftp = false;

        try {
            bftp = ftpx.checkConnectionWithOneRetry (
                    host,
                    user,
                    password,
                    iport );
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("Connection checkConnectionWithOneRetry ............................ "+bftp);

//        try {
//            nb_files = ftpx.FTPNbFiles(ftpPath+"seobot_images/");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        int l;
        String img_name = "";
        try {
            l = r.nextInt(nb_images);
            System.out.println( "l .................................................. " + l);
            img_name = img_names[l];
        } catch (Exception e) {
            try {
                ftpx.checkConnectionWithOneRetry (
                        host,
                        user,
                        password,
                        iport );
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
        System.out.println( "img_name ........................................... " + img_name);

//                        long t=System.currentTimeMillis();
//                        while(System.currentTimeMillis()<t+1000L*d*d){}
//                        ftpx.checkConnectionWithOneRetry (
//                                host,
//                                user,
//                                password,
//                                iport );
//                        nb_files = ftpx.FTPNbFiles(ftpPath + "seobot_images/");

        System.out.println( " .............................................. img_name : "+img_name);

        String path_img = ftpPath + "seobot_images/"+ img_name;

        byte[] img_content = new byte[0];
        try {
            img_content = ftpx.dwnld_img(path_img);
        } catch (Exception e) {
            e.printStackTrace();
            try {
                ftpx.checkConnectionWithOneRetry (
                        host,
                        user,
                        password,
                        iport );
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }

        String new_img_name = directory_images+System.currentTimeMillis() +
                img_name.substring((img_name.length()-5));

        String new_path_img = ftpPath + new_img_name;
        try {
            ftpx.upld_img(img_content,new_path_img);
        } catch (Exception e) {
//                System.out.println( "ftpUploadMemoryImg ...Exception.............................. "+e);
            try {
                ftpx.checkConnectionWithOneRetry (
                        host,
                        user,
                        password,
                        iport );
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }

        try {
            page_template = xtr.extractVAR("varimage", new_img_name, page_template);
        } catch (Exception e) {
            e.printStackTrace();
            try {
                ftpx.checkConnectionWithOneRetry (
                        host,
                        user,
                        password,
                        iport );
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }

//            System.out.println( "page_template ..................................... " + page_template);




        try {
            for (String[] strings : table_synonym) {
                if (strings[0] == null || strings[1] == null) continue;
                do {
                    l = r.nextInt(strings.length - 1) + 1;
                } while (strings[l] == null);
                text = text.replace((strings[0] + " "), (strings[l] + " "));
                text = text.replace((strings[0] + "s "), (strings[l] + "s "));
            }
        } catch (Exception e) {
            e.printStackTrace();
            try {
                ftpx.checkConnectionWithOneRetry (
                        host,
                        user,
                        password,
                        iport );
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }




        try {
            for(int i = 2;i<9;i++) {
                int j = r.nextInt(8)+2;
                text=text.replace(""+i,""+j);
//                System.out.println("replace number ................................. "+i+" "+j);
            }
            for (int i =0; i<(words_replace.length-1); i=i+2){
                text=text.toLowerCase().replace(words_replace[i],words_replace[i+1]);
//                System.out.println("words_replace[i],words_replace[i+1] .................... "+
//                        words_replace[i]+" "+words_replace[i+1]);
            }
        } catch (Exception e) {
            e.printStackTrace();
            try {
                ftpx.checkConnectionWithOneRetry (
                        host,
                        user,
                        password,
                        iport );
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }


        String new_page_vartitle = "";
        try {
            new_page_vartitle = xtr.extractVAR("vartitle", pageTitle, page_template);
        } catch (Exception e) {
            e.printStackTrace();
            try {
                ftpx.checkConnectionWithOneRetry (
                        host,
                        user,
                        password,
                        iport );
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }

        String new_page_vardescription = "";
        try {
            new_page_vardescription = xtr.extractVAR("vardescription", pageDescription,
                    new_page_vartitle);
        } catch (Exception e) {
            e.printStackTrace();
            try {
                ftpx.checkConnectionWithOneRetry (
                        host,
                        user,
                        password,
                        iport );
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }

        String new_page_vartext = "";
        try {
            new_page_vartext = xtr.extractVAR("vartext", text, new_page_vardescription);
        } catch (Exception e) {
            e.printStackTrace();
            try {
                ftpx.checkConnectionWithOneRetry (
                        host,
                        user,
                        password,
                        iport );
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }


//        System.out.println("new_page_vartext ...................................... "+new_page_vartext);


        try {
            if (pageUrl.length() < 6) {                             //if (pageUrl.equals("") || pageUrl.length()<6) {
                int n = r.nextInt((6 - 3) + 1) + 3;
                StringBuilder pageUrlBuilder = new StringBuilder(pageUrl);
                for (int i = 1; i < n + 1; i++) {
                    int nb = r.nextInt((wu.length - 1) + 1);
                    pageUrlBuilder.append(" ").append(wu[nb]);  //pageUrl = pageUrl + "_" + wu[nb];
                }
                pageUrl = pageUrlBuilder.toString();
                pageUrl = pageUrl.substring(1);
            }
            pageUrl=pageUrl.replace("&", " ");
            pageUrl=pageUrl.replace(";", " ");
            pageUrl=pageUrl.replace(".", " ");
            pageUrl=pageUrl.replace(",", " ");

            pageUrl=System.currentTimeMillis()+pageUrl;
            if(pageUrl.length()>1960)pageUrl=pageUrl.substring(0,1959);
            pageUrl = pageUrl + ".html";
        } catch (Exception e) {
            e.printStackTrace();
            try {
                ftpx.checkConnectionWithOneRetry (
                        host,
                        user,
                        password,
                        iport );
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }

        System.out.println("pageUrl ________________________________________________________ "+pageUrl);


        String url_manager = "";
        try {
            url_manager = URLManager(
                    user,
                    password,
                    host,
                    port,
                    ftpPath,
                    directory_preferences,
                    page_link,
                    pageTitle,
                    pageUrl);
        } catch (Exception e) {
            e.printStackTrace();
            try {
                ftpx.checkConnectionWithOneRetry (
                        host,
                        user,
                        password,
                        iport );
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }

//        System.out.println("link ....................................................... "+url_manager);

        if(url_manager==null)url_manager="";
        String new_page_varlink = "";
        try {
            new_page_varlink = new_page_vartext.replace("varlink",url_manager);
        } catch (Exception e) {
            e.printStackTrace();
            ftpx.checkConnectionWithOneRetry (
                    host,
                    user,
                    password,
                    iport );
        }

        //xtr.extractVAR("varlink", link, new_page_vartext);
//        System.out.println("new_page_varlink ...................................... "+new_page_varlink);

        try {
            if(!pageUrl.endsWith(".html"))pageUrl=pageUrl+".html";
        } catch (Exception e) {
            e.printStackTrace();
        }

        String path_main_final_page = ftpPath + pageUrl;
//        System.out.println( "....................................................................... " +
//                user +" "+
//                password +" "+ new_page_varlink +" "+ host +" "+ port +" "+ path_main_final_page);

        try {
            ftpx.upld( new_page_varlink , path_main_final_page );
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    String URLManager(String user, String password, String host, String port,
                      String ftpPath,
                      String directory_preferences, String page_link,
                      String pageTitle, String pageUrl) {

        //read existing url list
        String path_pref_urlList = ftpPath + directory_preferences + "urlList.txt";
        try {
            urlList = ftpx.dwnld(path_pref_urlList);
        } catch (Exception e) {
            urlList = "";
//            System.out.println("Exception urlList.txt : "+e);
        }
//        System.out.println("urlList .................................................................");


        String newUrlList = "<ul><a href=\"" + pageUrl + "\">" + pageTitle + "</a></ul>" + urlList;

//        System.out.println(" newUrlList : "+newUrlList);

        try {
            ftpx.upld(
                    newUrlList,
                    path_pref_urlList);
        } catch (Exception e) {
            e.printStackTrace();
        }

        String path_pref_link = ftpPath + directory_preferences + page_link;

//        System.out.println("........................................ path_pref_link : "+path_pref_link);
        String page_link_content;
        try {
            page_link_content = ftpx.dwnld(path_pref_link);
//            System.out.println(".............................. page_link_content : "+page_link_content);
        } catch (Exception e) {
            page_link_content = "";
//            System.out.println("Exception page_link_content : "+e);
        }

        String new_page_link_content = "";
        try {
            new_page_link_content = page_link_content.replace("varlink",urlList);
        } catch (Exception e) {
            e.printStackTrace();
        }

//        System.out.println("---------------------------new_page_link_content : "+new_page_link_content);

        String path_main_link = ftpPath + page_link;

        try {
            ftpx.upld( new_page_link_content,path_main_link );
        } catch (Exception e) {
            e.printStackTrace();
        }

//        System.out.println("urlList : "+urlList);

        return urlList;
    }
}
