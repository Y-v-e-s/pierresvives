package com.ruffenach.pierresvives.seo.dataprocessing.extraction;

import org.springframework.stereotype.Service;
/**
 * Class to replace a variable by another in a webpage
 */
@Service
public class ExtractVAR {

    private boolean stopthreading = true;
    public void setStopthreading(boolean b) {
        this.stopthreading = b;
    }

    public String extractVAR(String oldvar, String newvar, String webPage) {
        StringBuilder str = new StringBuilder();

        boolean bool;

        for (int i = 0; i < webPage.length(); i++) {

            if(!stopthreading) break;

            bool = true;
            for (int ii = 0; ii < oldvar.length(); ii++) {
                if ((i + ii) >= webPage.length()) {
                    bool = false;
                    break;
                }
                if (webPage.toLowerCase().charAt(i + ii) == oldvar.toLowerCase().charAt(ii)) {
                    bool = bool && true;
                } else {
                    bool = bool && false;
                }
            }
            if (bool == false) {
                str.append(webPage.charAt(i));
            }
            if (bool == true) {
                str.append(newvar);
                i = i + oldvar.length() - 1;
            }
        }
        return str.toString();
    }
}
