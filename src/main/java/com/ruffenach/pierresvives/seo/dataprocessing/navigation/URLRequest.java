package com.ruffenach.pierresvives.seo.dataprocessing.navigation;

import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
/**
 * Class to download a web page from a URL.
 */
@Service
public class URLRequest {

    private static int numberofconsecutiveexception = 0;
    private static boolean consecutive = false;

//    @Autowired
//    SEOBotSettingsSessionService seoBotSettingsDTO;

    public String urlRequest(String url) {
        //String url = seoBotSettingsDTO.getUrlforbot();
        StringBuilder result = new StringBuilder();
        try {
            HttpURLConnection connection;
//			         String url = URLEncoder.encode(quest, "UTF-8") + "=";
            connection = (HttpURLConnection) new URL(url).openConnection();
            BufferedReader inflow =
                    new BufferedReader(
                            new InputStreamReader(
                                    connection.getInputStream(),
                                    StandardCharsets.UTF_8));

            String line;
            while ((line = inflow.readLine()) != null) {
                result.append(line).append("\n");   //result += line + "\n";
            }
            connection.getInputStream().close();
        } catch (IOException e) {
            numberofconsecutiveexception++;
            consecutive = true;

            System.out.println("URLRequest : IOException URLRequest");
            if (numberofconsecutiveexception > 100) {
                numberofconsecutiveexception = 0;
                consecutive = false;
                return "stopharvesting";
            }
            return "";
        }

        if (consecutive) {
            numberofconsecutiveexception--;
            consecutive = false;
        }

        return result.toString();
    }
}