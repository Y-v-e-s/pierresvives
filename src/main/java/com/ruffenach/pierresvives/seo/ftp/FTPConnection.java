package com.ruffenach.pierresvives.seo.ftp;
import java.io.*;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPConnectionClosedException;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.springframework.stereotype.Component;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
/**
 * Annotated class that offers and manages various tools for connecting,
 * controlling, downloading and uploading data via FTP.
 *
 * @Component is an annotation that allows Spring to automatically detect the bean.
 * Spring will scan the application for classes annotated with @Component,
 * instantiate them and inject any specified dependencies into them,
 * inject these beans wherever needed.
 *
 */
@Component
public class FTPConnection {

    public FTPClient aFTPClient = null;

    ///////////////////////////////////////////////////////////////////////////////////////////////////FTPConnection

    public boolean FTPConnect(String host, String username, String password, int port) {
        try {
            aFTPClient = new FTPClient();
            aFTPClient.connect(host, port);
            if (FTPReply.isPositiveCompletion(aFTPClient.getReplyCode())) {
                boolean status = aFTPClient.login(username, password);
                //ASCII_FILE_TYPE,BINARY_FILE_TYPE,EBCDIC_FILE_TYPE,BINARY_FILE_TYPE
                aFTPClient.setFileType(FTP.BINARY_FILE_TYPE);
                aFTPClient.enterLocalPassiveMode();
                System.out.println("........................................... connection succeeded");
                return status;
            }
        } catch (Exception e) {
            System.out.println("******************************************* Connection failed : " + e);
        }
        return false;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////FTPDisconnection

    public boolean FTPDisconnect() {
        try {
            aFTPClient.logout();
            aFTPClient.disconnect();
            return true;
        } catch (Exception e) {
            System.out.println("Exception .................................................................. " + e);
        }
        return false;
    }

        /*
        //méthode pour déterminer, récupérer le dossier, répertoire de travail courant
        public String ftpGetCurrentWorkingDirectory() {
        try {
            String workingDir = aFTPClient.printWorkingDirectory();
            return workingDir;
        } catch (Exception e) {
            System.out.println("Détermination du dossier de travail courant impossible.");
        }
        return null;
        }

        public boolean ftpChangeDirectory(String directory_path) {
        try {
            System.out.println("Chemin du dossier/répertoire : " + directory_path);
            aFTPClient.changeWorkingDirectory(directory_path);
        } catch (Exception e) {
            System.out.println("Le changement de dossier/répertoire a échoué." + e);
        }
        return false;
        }
        */

    ///////////////////////////////////////////////////////////////////////////////////////////////////////listFiles

    public String[] FTPListFiles(String dir_path) {
        System.out.println("FTPListFiles ................................................. " + dir_path);
        String[] fileList = null;
        try {
            FTPFile[] ftpFiles = aFTPClient.listFiles(dir_path);
            int length = ftpFiles.length;
            fileList = new String[length];
            for (int i = 0; i < length; i++) {
                String name = ftpFiles[i].getName();
                boolean isFile = ftpFiles[i].isFile();
                if (isFile) {
                    fileList[i] = "File :: " + name + "\n";
                    System.out.println("File : " + name);
                } else {
                    fileList[i] = "Directory :: " + name + "\n";
                    System.out.println("Directory : " + name);
                }
            }
            return fileList;
        } catch (Exception e) {
            System.out.println("Exception FTPListFiles : " + e);
            fileList[0] = "Exception : " + e;
            return fileList;
        }
    }

    public String[] FTPNbIsFile(String dir_path) {
        System.out.println("FTPNbIsFile ................................................. " + dir_path);
        String[] fileList;
        int j = 0;
        try {
            FTPFile[] ftpFiles = aFTPClient.listFiles(dir_path);
            int length = ftpFiles.length;
            fileList = new String[length];
            System.out.println("length : " + length);
            for (int i = 0; i < length; i++) {
                boolean isFile = ftpFiles[i].isFile();
                if (isFile) {
                    fileList[j] = ftpFiles[i].getName();
                    j++;
                }
            }
            System.out.println("j : " + j);
            String[] tab = new String[j];
            for (int i = 0; i < j; i++) {
                tab[i] = fileList[i];
                System.out.println("tab[i] : " + tab[i]);
            }
            return tab;
        } catch (Exception e) {
            System.out.println("Exception FTPnbFiles : " + e);
            return null;
        }
    }

    /*
    public int FTPNbFiles (String dir_path) {
        try {
            FTPFile[] ftpFiles = aFTPClient.listFiles(dir_path);
            return ftpFiles.length;
        } catch (Exception e) {
            System.out.println("Exception FTPnbFiles : " + e);
            return 0;
        }
    }

    public String FTPNthFile (String dir_path,int nth) {
        try {
            FTPFile[] ftpFiles = aFTPClient.listFiles(dir_path);
            if (ftpFiles[nth].isFile()) {
                System.out.println("FTPNthFile : " + ftpFiles[nth].getName());
                return ftpFiles[nth].getName();
            }
        } catch (Exception e) {
            System.out.println("Exception FTPNthFile : " + e);
        }
        return "";
    }
    */


    public int FTPListFilesLength(String dir_path) {
        try {
            FTPFile[] ftpFiles = aFTPClient.listFiles(dir_path);
            int j = 0;
            for (FTPFile ftpFile : ftpFiles) {
                if (ftpFile.isFile()) {
//                    System.out.println("FTPListFilesLength : " + ftpFiles[i].getName());
                    j++;
                }
            }
            return j;
        } catch (Exception e) {
            System.out.println("Exception FTPListFilesLength : " + e);
        }
        return 0;
    }

    public String oldest_url(String dir_path) {
        boolean b = true;
        long oldest = 0, l;
        FTPFile[] ftpFiles;
        try {
            ftpFiles = aFTPClient.listFiles(dir_path);
            for (FTPFile ftpFile : ftpFiles) {
                if (ftpFile.isFile()) {
                    l = Long.parseLong(ftpFile.getName());
                    if (b) {
                        oldest = l;
                        b = false;
                    } else {
                        if (l < oldest) {
                            oldest = l;
                        }
                    }
                }
            }
            return "" + oldest;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    ///////////////////////////////////////////////////////////////// method to create new directory

        /*
        public boolean FTPMakeDirectory (String new_dir_path) {
        try {
            return aFTPClient.makeDirectory(new_dir_path);
        } catch (Exception e) {
            System.out.println("Error: could not create new directory named " + new_dir_path);
        }
        return false;
        }
        */

    /////////////////////////////////////////////////////////////////// method to remove a directory
    
        /*    
        public boolean FTPRemoveDirectory (String dirPath) {
        try {
            return aFTPClient.removeDirectory(dirPath);
        } catch (Exception e) {
            System.out.println( "Error: could not remove directory named " + dirPath);
        }
        return false;
        }
        */

    //////////////////////////////////////////////////////////////////////// method to remove a file

    public void FTPRemoveFile(String filePath) {
        try {
            aFTPClient.deleteFile(filePath);
        } catch (Exception e) {
            System.out.println("FTPRemoveFile Exception : " + e);
        }
    }

    //////////////////////////////////////////////////////////////////////// method to rename a file

        /*

        public boolean ftpRenameFile(String from, String to) {
        try {
            return aFTPClient.rename(from, to);
        } catch (Exception e) {
            System.out.println("Could not rename file: " + from + " to: " + to);
        }
        return false;
        }
        */
    
        /*
        public boolean ftpDownload(String srcFilePath, String desFilePath) {
        srcFilePath="/1001alsaces.com/images/image_page.png";
        desFilePath="/image_page.png";
        boolean status = false;
        try {
            FileOutputStream desFileStream = new FileOutputStream(desFilePath);
            status = aFTPClient.retrieveFile(srcFilePath, desFileStream);
            desFileStream.close();
            return status;
        } catch (Exception e) {
            System.out.println("Download no");
        }

        return status;
        }
        */

    public String dwnld(String path) throws IOException {
        String s;
        OutputStream outputStream = new ByteArrayOutputStream();
        boolean success = aFTPClient.retrieveFile(path, outputStream);
        System.out.println("dwnld................................................" + success);
        s = outputStream.toString();
        outputStream.close();
        return s;
    }

    public byte[] dwnld_img(String ftpPath) throws IOException {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        boolean success = aFTPClient.retrieveFile(ftpPath, stream);
        System.out.println("dwnld_img............................................" + success);
        byte[] by = stream.toByteArray();
        stream.close();
        System.out.println("dwnld_img....................................................");
        return by;
    }

    public boolean upld(String memoryString, String ftpPath) throws IOException {
        InputStream inputStream =
                new ByteArrayInputStream(
                        memoryString.getBytes()); //StandardCharsets.UTF_8
        boolean success = aFTPClient.storeFile(ftpPath, inputStream);
        inputStream.close();
        return success;
    }

    public boolean upld_img(
            byte[] by,
            String ftpPath) throws IOException {
        InputStream inputStream = new ByteArrayInputStream(by);
        boolean success = aFTPClient.storeFile(ftpPath, inputStream);
        inputStream.close();
        return success;
    }

    /**
     * NOOP command : (No Operation command)
     * This command performs no action other than having the server send an OK reply.
     * .sendNoOp(); sends a NOOP command to the FTP server. This is useful for preventing server timeouts.
     * Returns: True if successfully completed, false if not.
     */
    public boolean checkConnectionWithOneRetry(String host,
                                               String username,
                                               String password,
                                               int port) {
        try {
            boolean answer = aFTPClient.sendNoOp();
            if (answer)
                return true;
            else {
                System.out.println("Server connection failed!");
                boolean success = FTPConnect(
                        host,
                        username,
                        password,
                        port);

                if (success) {
                    System.out.println("Reconnect attampt have succeeded!");
                    return true;
                } else {
                    System.out.println("Reconnect attampt failed!");
                    return false;
                }
            }
        } catch (FTPConnectionClosedException | NullPointerException e) {
            System.out.println("Server connection is closed!");
            boolean recon = FTPConnect(
                    host,
                    username,
                    password,
                    port);
            if (recon) {
                System.out.println("Reconnect succeeded");
                return true;
            } else {
                System.out.println("Reconnect failed");
                return false;
            }
        } catch (IOException e) {
            System.out.println("Server connection failed");
            boolean recon = FTPConnect(
                    host,
                    username,
                    password,
                    port);
            if (recon) {
                System.out.println("Reconnect succeeded");
                return true;
            } else {
                System.out.println("Reconnect failed");
                return false;
            }
        }
    }
}