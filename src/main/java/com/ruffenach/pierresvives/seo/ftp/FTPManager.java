package com.ruffenach.pierresvives.seo.ftp;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Class that offers and manages various tools for downloading and uploading data via FTP.
 */
public class FTPManager {

    public String ftpDownloadMemory(String user,
                                    String password,
                                    String host,
                                    String port,
                                    String ftpPath) {

        String s;

        final int BUFFER_SIZE = 4096;

        String ftpByUrl = "ftp://%s:%s@%s:%s/%s;type=i";

        ftpByUrl = String.format(ftpByUrl, user, password, host, port, ftpPath);

        System.out.println("**************************** Download URL : " + ftpByUrl);

        try {
            URL url = new URL(ftpByUrl);
            URLConnection connexion = url.openConnection();
            InputStream inputStream = connexion.getInputStream();
            OutputStream outputStream = new ByteArrayOutputStream();


            byte[] buffer = new byte[BUFFER_SIZE];
            int bytesRead;// = -1;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }

            s = outputStream.toString();
            System.out.println(s);

            outputStream.close();
            inputStream.close();

            System.out.println("File downloaded *******************************");
        } catch (IOException ex) {
            return "";
        }
        return s;
    }

    public void ftpUploadMemoryText(String user,
                                    String password,
                                    String memoryString,
                                    String host,
                                    String port,
                                    String ftpPath) {

        final int BUFFER_SIZE = 4096;

        String ftpByUrl = "ftp://%s:%s@%s:%s/%s;type=i";

        ftpByUrl = String.format(ftpByUrl, user, password, host, port, ftpPath);

        System.out.println("****************************** Upload URL : " + ftpByUrl);

        try {
            URL url = new URL(ftpByUrl);
            URLConnection connexion = url.openConnection();
            OutputStream outputStream = connexion.getOutputStream();
            InputStream inputStream =
                    new ByteArrayInputStream(
                            memoryString.getBytes()); //StandardCharsets.UTF_8

            byte[] buffer = new byte[BUFFER_SIZE];
            int bytesRead;// = -1;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }

            inputStream.close();
            outputStream.close();
        } catch (IOException e) {
            System.out.println("ftpUploadMemoryText IOException : " + e);
        }
        System.out.println("File uploaded *************************************");
    }

    public byte[] ftpDownloadMemoryImg(
            String user,
            String password,
            String host,
            String port,
            String ftpPath) {

//					String s="";

        final int BUFFER_SIZE = 32768;

        String ftpByUrl = "ftp://%s:%s@%s:%s/%s;type=i";

        ftpByUrl = String.format(ftpByUrl, user, password, host, port, ftpPath);

        System.out.println("**************************** Download URL : " + ftpByUrl);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        byte[] by = null;

        try {
            URL url = new URL(ftpByUrl);
            URLConnection connexion = url.openConnection();
            InputStream inputStream = connexion.getInputStream();

            byte[] buffer = new byte[BUFFER_SIZE];
            int bytesRead;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
//			                outputStream.write(buffer, 0, bytesRead);
                stream.write(buffer, 0, bytesRead);
            }

            by = stream.toByteArray();
//			            s=outputStream.toString();
//			            System.out.println(s);

            ((OutputStream) stream).close();
            inputStream.close();

            System.out.println("File downloaded *******************************");
        } catch (IOException ex) {
            System.out.println("ftpDownloadMemoryImg IOException : " + ex);
        }
        return by;
    }


    public void ftpUploadMemoryImg(
            String user,
            String password,
            byte[] by,
            String host,
            String port,
            String ftpPath) {

        final int BUFFER_SIZE = 32768;

        String ftpByUrl = "ftp://%s:%s@%s:%s/%s;type=i";

        ftpByUrl = String.format(ftpByUrl, user, password, host, port, ftpPath);

        System.out.println("****************************** Upload URL : " + ftpByUrl);

        InputStream inputStream = new ByteArrayInputStream(by);
        try {
            URL url = new URL(ftpByUrl);
            URLConnection connexion = url.openConnection();
            OutputStream outputStream = connexion.getOutputStream();

            byte[] buffer = new byte[BUFFER_SIZE];
            int bytesRead;// = -1;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }

            inputStream.close();
            outputStream.close();
        } catch (IOException e) {
            return;
        }
        System.out.println("File uploaded *************************************");
    }

    public String ftpDownloadMemLink(String user,
                                     String password,
                                     String host,
                                     String port,
                                     String ftpPath) {

        String s;

        final int BUFFER_SIZE = 4096;

        String ftpByUrl = "ftp://%s:%s@%s:%s/%s;type=i";

        ftpByUrl = String.format(ftpByUrl, user, password, host, port, ftpPath);

        System.out.println("**************************** Download URL : " + ftpByUrl);

        try {
            URL url = new URL(ftpByUrl);
            URLConnection connexion = url.openConnection();
            InputStream inputStream = connexion.getInputStream();
            OutputStream outputStream = new ByteArrayOutputStream();


            byte[] buffer = new byte[BUFFER_SIZE];
            int bytesRead;// = -1;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }

            s = outputStream.toString();
            System.out.println(s);

            outputStream.close();
            inputStream.close();

            System.out.println("File downloaded *******************************");
        } catch (IOException ex) {
            s = "";
        }
        return s;
    }


    public void ftpUploadMemLink(String user,
                                 String password,
                                 String memoryString,
                                 String host,
                                 String port,
                                 String ftpPath) {

        final int BUFFER_SIZE = 4096;

        String ftpByUrl = "ftp://%s:%s@%s:%s/%s;type=i";

        ftpByUrl = String.format(ftpByUrl, user, password, host, port, ftpPath);

        System.out.println("****************************** Upload URL : " + ftpByUrl);

        try {
            URL url = new URL(ftpByUrl);
            URLConnection connexion = url.openConnection();
            OutputStream outputStream = connexion.getOutputStream();
            InputStream inputStream =
                    new ByteArrayInputStream(
                            memoryString.getBytes());

            byte[] buffer = new byte[BUFFER_SIZE];
            int bytesRead;// = -1;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }

            inputStream.close();
            outputStream.close();
        } catch (IOException e) {
            System.out.println("ftpUploadMemLink IOException : " + e);
        }
        System.out.println("File uploaded *************************************");
    }
}