package com.ruffenach.pierresvives.seo.repositories;

import com.ruffenach.pierresvives.seo.entities.SEOSettingsEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

/**
 * Interface that offers the necessary tools to use MongoDB and allow persistence of SEO settings.
 * Interface that extends MongoRepository<T,ID> and provides a set of tools for data manipulation.
 * The MongoRepository<T,ID> interface which extends CrudRepository <T,ID>,
 * PagingAndSortingRepository <T,ID>,
 * QueryByExampleExecutor <T>,
 * Repository <T,ID>
 *     is implemented in this project.
 */
public interface SEOSettingsRepository extends MongoRepository<SEOSettingsEntity,String> {
    Optional<SEOSettingsEntity> findById(String id);
    List<SEOSettingsEntity> findByUsername(String username);
}
