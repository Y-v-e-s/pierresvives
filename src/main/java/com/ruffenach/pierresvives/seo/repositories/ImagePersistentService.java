package com.ruffenach.pierresvives.seo.repositories;

import com.ruffenach.pierresvives.seo.entities.ImageEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;

/**
 * Persistent manager for accessing and manipulating image
 * Repository access class (interface grouping a set of tools to ensure data persistence,
 * to interact with the database).
 * This class includes the following methods:
 * create() to save image,
 * getAll() which returns a list of images,
 * getSize() which returns the size of the list.
 *
 * The repository associated with MongoDB does not offer an update :
 * so if the ids match, the previous record is overwritten.
 */
@Service
public class ImagePersistentService {

    @Autowired
    private ImageRepository repository;

//    public String create(String imgname, String imgaddress, String userid){
//        ImageEntity img = new ImageEntity(imgname, imgaddress, userid);
//        repository.save(img);
//        return img.getId();
//    }

    public String create(String imgname, String imgaddress, String username){
        ImageEntity img = new ImageEntity(imgname, imgaddress, username);
        repository.save(img);
        return img.getId();
    }

    public int getSize(String username){
        int i=getAllByUsername(username).size();
        return i;
    }

    public List<ImageEntity> getAllByImgnameAndUsername(String imgname, String username) {
        return repository.findAllByImgnameAndUsername(imgname, username);}
    public void deleteImage(String imgname, String username){
        repository.deleteAllByImgnameAndUsername(imgname, username);
    }
    public List<ImageEntity> getAllByUsername(String username) { return repository.findAllByUsername(username);}
}