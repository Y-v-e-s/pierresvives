package com.ruffenach.pierresvives.seo.repositories;

import com.ruffenach.pierresvives.seo.entities.ImageEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
/**
 * Interface that offers the necessary tools to use MongoDB and allow persistence of images.
 * Interface that extends MongoRepository<T,ID> and provides a set of tools for data manipulation.
 * The MongoRepository<T,ID> interface which extends CrudRepository <T,ID>,
 * PagingAndSortingRepository <T,ID>,
 * QueryByExampleExecutor <T>,
 * Repository <T,ID>
 *     is implemented in this project.
 */
public interface ImageRepository extends MongoRepository<ImageEntity,String> {
    List<ImageEntity> findAllByImgname(String imgname);
    List<ImageEntity> findAllByImgnameAndUsername(String imgname, String username);
    List<ImageEntity> findAllByUsername(String username);
    void deleteAllByImgnameAndUsername(String imgname, String username);
}
