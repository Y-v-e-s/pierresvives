package com.ruffenach.pierresvives.seo.repositories;

import com.ruffenach.pierresvives.seo.entities.SEOSettingsEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Persistent manager for accessing and manipulating SEO settings
 * Repository access class (interface grouping a set of tools to ensure data persistence,
 * to interact with the database).
 * This class includes the following methods:
 * create() to save SEO settings,
 * getById() which returns SEO settings from the Id.
 * The repository associated with MongoDB does not offer an update :
 * so if the ids match, the previous record is overwritten.
 */
@Service
public class SEOSettingsPersistentService {

    @Autowired
    SEOSettingsRepository repository;

    public String create(SEOSettingsEntity s) {
        repository.save(s);
        return s.getId();
    }

//    public void createWithId(String id, String linklist, String pagelinkfull) {
//        SEOSettingsSessionService s;
//        Optional<SEOSettingsSessionService> o = repository.findById(id);
//        if (o.isPresent()) {
//            s = o.get();
//            s.setLinklist(linklist);
//            s.setPagelinkfull(pagelinkfull);
//            repository.save(s);
//        }
//        else repository.save(seoSettingsDTO);
//    }

    public SEOSettingsEntity readById(String id) {
        Optional<SEOSettingsEntity> s = repository.findById(id);
        return s.orElseGet(SEOSettingsEntity::new);
    }

    /**
     * username is unique, should return only one result
     * @return SEO settings entity
     */
    public SEOSettingsEntity readByUsername(String username) {
        List<SEOSettingsEntity> s = repository.findByUsername(username);
        if (s.size()>0)return s.get(0);
        return new SEOSettingsEntity();
    }
}