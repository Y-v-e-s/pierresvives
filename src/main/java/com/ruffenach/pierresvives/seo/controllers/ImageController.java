package com.ruffenach.pierresvives.seo.controllers;
import com.ruffenach.pierresvives.dto.SEOSettingsSessionService;
import com.ruffenach.pierresvives.dto.UserSessionService;
import com.ruffenach.pierresvives.seo.entities.ImageEntity;
import com.ruffenach.pierresvives.seo.entities.SEOSettingsEntity;
import com.ruffenach.pierresvives.seo.repositories.ImagePersistentService;
import com.ruffenach.pierresvives.seo.repositories.SEOSettingsPersistentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import java.util.ArrayList;
import java.util.List;
/**
 * Class controller (web request handler) for images.
 * Class annotated by @Controller that maps get and post requests for path /image
 * In the class we have created two methods which returns to image and
 * are mapped to a GET or POST request thus any URL call ending with image
 * would be routed by the DispatcherServlet to one of the methods in this class.
 */
@Controller
public class ImageController {

    @Autowired
    ImagePersistentService imageDAO;

    @Autowired
    UserSessionService userDTO;
    /**
     * si utilisateur non enregistré, scope session
     */
    @Autowired
    SEOSettingsSessionService seoSettingsDTO;

    @Autowired
    SEOSettingsPersistentService seoSettingsDAO;

    @Value("${index.help13}")
    private String indexhelp13;
    @Value("${index.help14}")
    private String indexhelp14;
    @Value("${index.help15}")
    private String indexhelp15;
    @Value("${index.help16}")
    private String indexhelp16;

    SEOSettingsEntity seoSettingsEntity;
    String username;

    @GetMapping("/image")
    public String imageForm(Model model) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        username = authentication.getName();

        model.addAttribute("indexhelp13", indexhelp13);
        model.addAttribute("indexhelp14", indexhelp14);
        model.addAttribute("indexhelp15", indexhelp15);
        model.addAttribute("indexhelp16", indexhelp16);

        //seoSettingsEntity=seoSettingsDAO.readById(userDTO.getId());
        seoSettingsEntity=seoSettingsDAO.readByUsername(username);
        seoSettingsEntity.setUsername(username);

        model.addAttribute("liste", seoSettingsEntity.getImagelist());
        model.addAttribute("image", new ImageEntity());
        model.addAttribute("who", username);
        return "image";
    }

    @RequestMapping(value="/image", method= RequestMethod.POST, params="button=Save")
    public String save(@ModelAttribute ImageEntity imageModel, Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        username = authentication.getName();

        String imgname = imageModel.getImgname();
        String imgaddress = imageModel.getImgaddress();

        if (imgname != null && imgname.length() > 0 && imgaddress != null && imgaddress.length() > 0) {
            List<ImageEntity> liste=new ArrayList<>();
            ImageEntity im = new ImageEntity(imgname, imgaddress, username);
            if (seoSettingsEntity!=null)liste = seoSettingsEntity.getImagelist();
            liste.add(im);
            seoSettingsEntity.setImagelist(liste);

            seoSettingsDTO.entityToService(seoSettingsEntity);
//            String id = userDTO.getId();
//            if (id != null && !id.equals("")) {
//                imageDAO.create(imgname, imgaddress, id);
//                seoSettingsDAO.create(seoSettingsEntity);
//            }

                imageDAO.create(imgname, imgaddress, username);
                seoSettingsDAO.create(seoSettingsEntity);

            model.addAttribute("liste", liste);
        }
        model.addAttribute("image", imageModel);
        return "image";
    }

    /**
     * Method to delete image
     */
    @RequestMapping(value="/image", method= RequestMethod.POST, params="button=Delete")
    public String delete(@ModelAttribute ImageEntity imageModel, Model model) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        username = authentication.getName();

        String imgname = imageModel.getImgname();


        System.out.println("ImageController : imgname username... "+imgname+" "+username);

        if (imgname != null && imgname.length() > 0) {
            imageDAO.deleteImage(imgname,username);
            List<ImageEntity> liste=imageDAO.getAllByUsername(username);

            System.out.println("ImageController : liste... "+liste.toString());
            seoSettingsEntity.setImagelist(liste);

            //deprecated
            seoSettingsDTO.entityToService(seoSettingsEntity);

            seoSettingsDAO.create(seoSettingsEntity);

            model.addAttribute("liste", liste);
        }
        model.addAttribute("image", imageModel);
        return "image";
    }
}