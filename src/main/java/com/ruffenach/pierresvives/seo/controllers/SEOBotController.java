package com.ruffenach.pierresvives.seo.controllers;

import com.ruffenach.pierresvives.dto.ResourceDTO;
import com.ruffenach.pierresvives.dto.SEOSettingsSessionService;
import com.ruffenach.pierresvives.dto.UserSessionService;
import com.ruffenach.pierresvives.seo.dataprocessing.construction.ConstructManagerDB;
import com.ruffenach.pierresvives.seo.dataprocessing.navigation.URLRequest;
import com.ruffenach.pierresvives.seo.dataprocessing.production.Harvester;
import com.ruffenach.pierresvives.seo.entities.SEOSettingsEntity;
import com.ruffenach.pierresvives.seo.repositories.SEOSettingsPersistentService;
import com.ruffenach.pierresvives.seo.website.entities.ResourceEntity;
import com.ruffenach.pierresvives.seo.website.repositories.ResourceDAO;
import com.ruffenach.pierresvives.seo.website.repositories.UrlDAO;
import com.ruffenach.pierresvives.seo.website.repositories.WebPageDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.context.annotation.SessionScope;
import org.springframework.web.servlet.view.RedirectView;

import java.nio.channels.AsynchronousCloseException;
import java.util.Set;

/**
 * Runtime class for seobot, seobotworking and other pages.
 * This class extends Thread and executes each task in a separate thread.
 * This class is annotated by @Controller for the implementation of web pages.
 * The scope of the bean is the session.
 */
@Controller
@SessionScope
public class SEOBotController extends Thread {

    @Autowired
    UrlDAO urlDAO;

    @Autowired
    SEOSettingsSessionService seoSettingsDTO;

    @Autowired
    SEOSettingsPersistentService seoSettingsPersistentService;

    @Autowired
    Harvester harvesterDB;

    @Autowired
    ConstructManagerDB constructManagerDB;

    @Autowired
    URLRequest urlRequest;

    @Autowired
    ResourceDTO resourceDTO;

    @Autowired
    ResourceDAO resourceDAO;

    @Autowired
    UserSessionService userDTO;

    @Autowired
    WebPageDAO webPageDAO;

    private SEOSettingsEntity seoSettingsEntity;
    public SEOSettingsEntity getSeoSettingsEntity() {
        return seoSettingsEntity;
    }
    public void setSeoSettingsEntity(SEOSettingsEntity seoSettingsEntity) {
        this.seoSettingsEntity = seoSettingsEntity;
    }

    private Authentication authentication;
    private String username;

    private volatile long threadId = -1;
    private int whichThread = 0;

    private boolean goseobot=false;
    public boolean isgoseobot() {
        return goseobot;
    }
    public void setgoseobot(boolean goseobot) {
        this.goseobot = goseobot;
    }

    public Authentication getAuthentication() {
        return authentication;
    }
    public void setAuthentication(Authentication authentication) {
        this.authentication = authentication;
    }

    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    public long getThreadId() {
        return threadId;
    }
    public void setThreadId(long threadId) {
        this.threadId = threadId;
    }

    public int getWhichThread() {
        return whichThread;
    }
    public void setWhichThread(int whichThread) {
        this.whichThread = whichThread;
    }

    private ResourceEntity rEntity = new ResourceEntity();

    @Value("${seobotworking.title.msg}")
    private String ntitle;
    @Value("${nano.msg}")
    private String mnano;
    @Value("${showallurlindb.title.msg}")
    private String mtitle;
    @Value("${id.msg}")
    private String mid;
    @Value("${url.msg}")
    private String murl;
    @Value("${index.msg}")
    private String mindex;

    @GetMapping("/seobot")
    public String seobotForm(Model model) {
        goseobot=false;
        authentication = SecurityContextHolder.getContext().getAuthentication();
        username = authentication.getName();

        SEOSettingsEntity seoSettingsEntity=seoSettingsPersistentService.readByUsername(username);
        seoSettingsDTO.entityToService(seoSettingsEntity);

        model.addAttribute("seobot", seoSettingsDTO);//new SEOBotSettingsModel());
        model.addAttribute("dto", seoSettingsDTO);
        model.addAttribute("howmanyurlindb", urlDAO.getSize(username));
        model.addAttribute("howmanyresourceindb", resourceDAO.getSize(username));
        model.addAttribute("howmanywebpageindb", webPageDAO.getSize(username));
        model.addAttribute("who", userDTO);
        model.addAttribute("whichthread", whichThread);
        model.addAttribute("username", username);
        return "seobot";
    }

    /**
     * Post method for processing the seobot form and launching a thread of execution.
     * A method of browsing the web and finding data. It is the tool often called crawler or spider.
     *
     * About Thread class, run() and start() methods:
     * To start a thread, we call the start() method of the java.lang.Thread class.
     * The start() method calls the run() method of the Runnable interface.
     * If we call the run() method directly,
     * the code of the run() method is executed in the same thread that calls the run method.
     * it will not create new thread until we call start() method.
     * If we call the Thread.start() method,
     * the code from the run() method is executed in a new thread created by the start() method.
     * We can call the run() method multiple times,
     * but we cannot call the start() method on the same thread instance.
     */
    @PostMapping("/seobot")
    public String seobotSubmit(@ModelAttribute SEOSettingsSessionService seobot,
                               Model model) {
        authentication = SecurityContextHolder.getContext().getAuthentication();
        username = authentication.getName();
        harvesterDB.setNotStopThread(true);

        boolean switchsoup = seobot.getSwitchsoup();
        String urlforbot = seobot.getUrlforbot();

        if (urlforbot != null && urlforbot.length() > 5) {
            seoSettingsDTO.setSwitchsoup(switchsoup);
            seoSettingsDTO.setUrlforbot(urlforbot);

            //démarrer thread
            if (whichThread == 0) {

                //changement de scope, de sessionscope à thread...
                if (userDTO.getId() != null) harvesterDB.setUserId(userDTO.getId());
                if (seoSettingsDTO.getUrlforbot() != null) harvesterDB.setUrl(seoSettingsDTO.getUrlforbot());
                harvesterDB.setSwitchSoup(seoSettingsDTO.getSwitchsoup());
                if (seoSettingsDTO.getSynonyms() != null) harvesterDB.setSynonyms(seoSettingsDTO.getSynonyms());
                if (seoSettingsDTO.getWordstouse() != null) harvesterDB.setWordstouse(seoSettingsDTO.getWordstouse());

                whichThread = 1;
                if (threadId == -1) {
                    Set<Thread> set = Thread.getAllStackTraces().keySet();
                    System.out.println("in whichThread == 0... whichThread = 1... in threadId == -1... ");
                    System.out.println("whichThread "+whichThread+" threadId "+threadId+" nb of thread "+set.size());
//                    //for (Thread thread : set) {
//                    if (set.size() > 0) run();
//                    else
                        start();/////////////////////////////////////////////////////////////////////gros problème
                } else {
                    new Thread(this).start();
                }
            }

            model.addAttribute("seobot", seobot);
            model.addAttribute("dto", seoSettingsDTO);
            model.addAttribute("howmanyurlindb", urlDAO.getSize(username));
            model.addAttribute("howmanyresourceindb", resourceDAO.getSize(username));
            model.addAttribute("howmanywebpageindb", webPageDAO.getSize(username));
            model.addAttribute("who", userDTO);
            model.addAttribute("whichthread", whichThread);
            return "red";
        }
        model.addAttribute("seobot", seobot);
        model.addAttribute("dto", seoSettingsDTO);
        model.addAttribute("who", userDTO);
        model.addAttribute("whichthread", whichThread);
        model.addAttribute("username", username);
        return "seobot";
    }

    /**
     * Get method to build new pages from saved data by launching a thread of execution.
     */
    @GetMapping("/build")
    public String build(@ModelAttribute SEOSettingsSessionService seobot, Model model) {

        authentication = SecurityContextHolder.getContext().getAuthentication();
        username = authentication.getName();

        constructManagerDB.setUsername(username);

        //true : thread possible
        //harvesterDB.setNotStopThread(true);

        if (whichThread == 0 && goseobot==true) {
            model.addAttribute("seobot", seobot);
            model.addAttribute("dto", seoSettingsDTO);
            model.addAttribute("howmanyurlindb", urlDAO.getSize(username));
            model.addAttribute("howmanyresourceindb", resourceDAO.getSize(username));
            model.addAttribute("howmanywebpageindb", webPageDAO.getSize(username));
            model.addAttribute("who", userDTO);
            model.addAttribute("whichthread", whichThread);
            model.addAttribute("username", username);
            return "seobot";
        }

        //démarrer thread
        if (whichThread == 0) {
            whichThread = 2;
            if (threadId == -1) {

                Set<Thread> set = Thread.getAllStackTraces().keySet();
                System.out.println("inbuild whichThread "+whichThread+" threadId "+threadId+" set.size() "+set.size());

//                //for (Thread thread : set) {
//                if (set.size() > 0) run();
//                else
                    start();/////////////////////////////////////////////////////////////////////gros problème
            } else {
                new Thread(this).start();
            }
        }

        model.addAttribute("seobot", seobot);
        model.addAttribute("dto", seoSettingsDTO);
        model.addAttribute("howmanyurlindb", urlDAO.getSize(username));
        model.addAttribute("howmanyresourceindb", resourceDAO.getSize(username));
        model.addAttribute("howmanywebpageindb", webPageDAO.getSize(username));
        model.addAttribute("who", userDTO);
        model.addAttribute("whichthread", whichThread);
        model.addAttribute("username", username);
        return "build";
    }

    /**
     * Get method for publishing new pages from the DB to the client's website via FTP.
     */
    @GetMapping("/publish")
    public RedirectView publish() {

        authentication = SecurityContextHolder.getContext().getAuthentication();
        username = authentication.getName();

        webPageDAO.publish(username);
        return new RedirectView("seobot");
    }

    /**
     * Get method of handling the page that is visible while the bot is working.
     * This is the page that succeeds the seobot page.
     */
    @GetMapping("/seobotworking")
    public String seobotworkingGet(Model model) {
        seoSettingsDTO.setThreadid(threadId);
        model.addAttribute("seobot", seoSettingsDTO);
        model.addAttribute("dto", seoSettingsDTO);
        model.addAttribute("howmanyurlindb", urlDAO.getSize(username));
        model.addAttribute("ntitle", mtitle);
        model.addAttribute("murl", murl);
        if (whichThread != 0) model.addAttribute("showallsortedurlindb", urlDAO.getAllSortedDesc(username));
        model.addAttribute("resourcedto", rEntity);
        model.addAttribute("resourcedao", resourceDAO.getAllSortedDesc(username));
        model.addAttribute("howmanyresourceindb", resourceDAO.getSize(username));
        model.addAttribute("whichthread", whichThread);
        model.addAttribute("username", username);
        return "seobotworking";
    }

    /**
     * Get method to give the order to delete all urls in the db
     */
    @GetMapping("/deleteallurlindb")
    public RedirectView deleteallurlindb() {
        urlDAO.deleteAllURLInDB(username);
        return new RedirectView("seobot");
    }

    /**
     * Get method to give the order to delete all resources in the db
     */
    @GetMapping("/deleteallresourceindb")
    public RedirectView deleteallresourceindb() {
        resourceDAO.deleteAllResourceInDB(username);
        return new RedirectView("seobot");
    }

    /**
     * Get method to give the order to delete all new pages in the db
     */
    @GetMapping("/deleteallnewpageindb")
    public RedirectView deleteallnewpageindb() {
        webPageDAO.deleteAllNewWebPageInDB();
        return new RedirectView("seobot");
    }

    /**
     * Get method to display all urls contained in the db
     */
    @GetMapping("/getallurlindb")//////////////////////////////////////////////////////////?
    public String getallurlindb(Model model) {
        model.addAttribute("showallurlindb", urlDAO.getAll(username));
        return "showallurlindb";
    }

    /**
     * Get method to display all urls contained in the db
     */
    @GetMapping("/showallurlindb")
    public String showallurlindb(Model model) {
        model.addAttribute("showallurlindb", urlDAO.getAllSortedDesc(username));
        model.addAttribute("mtitle", mtitle);
        model.addAttribute("mid", mid);
        model.addAttribute("murl", murl);
        model.addAttribute("mindex", mindex);
        model.addAttribute("mnano", mnano);
        return "showallurlindb";
    }

    /**
     * Get method to display all new pages contained in the db
     */
    @GetMapping("/showallnewpageindb")
    public String showallnewpageindb(Model model) {
        model.addAttribute("showallnewpageindb", webPageDAO.getAll(username));
        model.addAttribute("mtitle", mtitle);
        model.addAttribute("mid", mid);
        model.addAttribute("murl", murl);
        model.addAttribute("mindex", mindex);
        model.addAttribute("mnano", mnano);
        return "showallnewpageindb";
    }

    /**
     * Get method to display all resources contained in the db
     */
    @GetMapping("/showallresourceindb")
    public String showallresourceindb(Model model) {
        model.addAttribute("showallresourceindb", resourceDAO.getAllSortedDesc(username));
        model.addAttribute("mtitle", mtitle);
        model.addAttribute("mid", mid);
        model.addAttribute("murl", murl);
        model.addAttribute("mindex", mindex);
        model.addAttribute("mnano", mnano);
        return "showallresourceindb";
    }

    /**
     * Get method to stop page harvesting thread
     * This method broadcasts a boolean to stop the various processes before the thread actually stops
     * if boolean is false then stop thread
     * Note that this method is indirectly activated by seobotworking which is the page displayed during harvesting
     *
     * @return "redworktoindex" : go to redworktoindex (to finally return to the seobot page)
     */
    @GetMapping("/stopharvesting")
    public String stopharvesting() {
        harvesterDB.setNotStopThread(false);
        whichThread = 0;
        threadId = -1;
        return "redworktoindex";
    }

    /**
     * This method terminates the work of the page harvesting thread.
     * It is activated by the stopharvesting() method.
     *
     * @return "seobot" : return to the seobot page
     */
    @GetMapping("/redworktoindex")
    public String redworktoindex() {
        Set<Thread> set = Thread.getAllStackTraces().keySet();
        System.out.println("nb of thread "+set.size());
        for (Thread thread : set) {
            System.out.println("thread id "+thread.getId());
            if (thread.getId() == threadId) {
                System.out.println("in thread.getId() == threadId... thread id "+thread.getId());
                thread.interrupt();
                thread.isInterrupted();
                while (thread.isAlive()) {
                    System.out.println("in thread.isAlive()... "+thread.getId());
                    thread.interrupt();
                    thread.isInterrupted();
                }
            }
        }
        return "seobot";
    }

    /**
     * Thread's run method.
     * This method starts two different threads,
     * the page harvester threads or the page builder threads depending on the value of the integer whichThread.
     */
    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            threadId = Thread.currentThread().getId();
            System.out.println("run()...while...threadId "+threadId+" whichThread "+whichThread);
            if (whichThread == 0) {
                Thread.currentThread().interrupt();
                while (!Thread.currentThread().isInterrupted()) {
                    threadId = Thread.currentThread().getId();
                    System.out.println("run()...while...while...threadId "+threadId+" whichThread "+whichThread);
                    Thread.currentThread().interrupt();
                }
            }
            if (whichThread == 1) {
                System.out.println("in run()... in whichThread==1... ");
                rEntity = harvesterDB.harvesting(username);
            }
            if (whichThread == 2) {
                System.out.println("run()...whichThread==2...resourceDAO.getSize(username) "
                        +resourceDAO.getSize(username));
                //if there is enough resources to build pages
                if (resourceDAO.getSize(username) > 1) constructManagerDB.builder(username);
                //not enough resources to build pages, stop thread
                else {
                    Thread.currentThread().interrupt();
                    while (!Thread.currentThread().isInterrupted()) {
                        Thread.currentThread().interrupt();
                    }
                    whichThread = 0;
                    threadId = -1;
                    goseobot=true;
                    System.out.println("run()...goseobot=true");
                }
            }
        }
    }
}
