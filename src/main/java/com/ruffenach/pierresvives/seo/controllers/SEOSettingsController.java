package com.ruffenach.pierresvives.seo.controllers;

import com.ruffenach.pierresvives.dto.UserSessionService;
import com.ruffenach.pierresvives.dto.SEOSettingsSessionService;
import com.ruffenach.pierresvives.registration.repositories.RegistrationService;
import com.ruffenach.pierresvives.seo.entities.SEOSettingsEntity;
import com.ruffenach.pierresvives.seo.repositories.SEOSettingsPersistentService;
import com.ruffenach.pierresvives.user.entities.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
/**
 * Class controller (web request handler) for SEO settings.
 * Class annotated by @Controller that maps get and post requests for path /seosettings
 * In the class we have created two methods which returns to seosettings and
 * are mapped to a GET or POST request thus any URL call ending with seosettings
 * would be routed by the DispatcherServlet to one of the methods in this class.
 */
@Controller
public class SEOSettingsController {

    @Autowired
    RegistrationService registrationService;

    @Autowired
    UserSessionService userDTO;

    @Autowired
    SEOSettingsSessionService seoSettingsDTO;

    @Autowired
    SEOSettingsPersistentService seoSettingsDAO;

    @Value("${index.help20}")
    private String indexhelp20;
    @Value("${index.help21}")
    private String indexhelp21;
    @Value("${index.help22}")
    private String indexhelp22;
    @Value("${index.help23}")
    private String indexhelp23;
    @Value("${index.help24}")
    private String indexhelp24;

    SEOSettingsEntity seoSettingsModel;//=new SEOSettingsEntity();
    String username;
    UserEntity userEntity;

    @GetMapping("/seosettings")
    public String getSeoSettingsForm(Model model) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        username = authentication.getName();

        model.addAttribute("indexhelp20", indexhelp20);
        model.addAttribute("indexhelp21", indexhelp21);
        model.addAttribute("indexhelp22", indexhelp22);
        model.addAttribute("indexhelp23", indexhelp23);
        model.addAttribute("indexhelp24", indexhelp24);

        seoSettingsModel=seoSettingsDAO.readByUsername(username);
        //seoSettingsModel=seoSettingsDAO.readById(registrationService.getByUsername(username).getId());

        userEntity=registrationService.getByUsername(username);

        userDTO.entityToDTO(userEntity);

//        if (userDTO.getId() != null && !userDTO.getId().equals("")) {
//            seoSettingsModel=seoSettingsDAO.readById(userDTO.getId());
//        }
        model.addAttribute("seoset", seoSettingsModel);
        model.addAttribute("who", userDTO);
        return "seosettings";
    }

    @PostMapping("/seosettings")
    public String postSeoSettingsSubmit(@ModelAttribute SEOSettingsEntity seoset, Model model) {
        String ftpHost = seoset.getFtphost();
        String ftpLogin = seoset.getFtplogin();
        String ftpPwd = seoset.getFtppwd();
        int ftpPort = seoset.getFtpport();
        String ftpDir = seoset.getFtppathtomaindir();
        String pagetemplate = seoset.getPagetemplate();
        String pagelinkempty = seoset.getPagelinkempty();
        String linklist = seoset.getLinklist();
        String wordtouse = seoset.getWordtouse();
        String wordtoreplace = seoset.getWordtoreplace();
        String wordtoremove = seoset.getWordtoremove();
        String synonym = seoset.getSynonym();

        //if settings are OK
        if (
                ftpHost != null && ftpHost.length() > 0
                        && ftpLogin != null && ftpLogin.length() > 0
                        && ftpPwd != null && ftpPwd.length() > 0
                        && ftpPort >= 0
        ) {
            seoSettingsModel.setFtphost(ftpHost);
            seoSettingsModel.setFtplogin(ftpLogin);
            //à crypter:
            seoSettingsModel.setFtppwd(ftpPwd);
            seoSettingsModel.setFtpport(ftpPort);
            seoSettingsModel.setFtppathtomaindir(ftpDir);
            seoSettingsModel.setPagetemplate(pagetemplate);
            seoSettingsModel.setPagelinkempty(pagelinkempty);
            seoSettingsModel.setLinklist(linklist);
            seoSettingsModel.setWordtouse(wordtouse);
            seoSettingsModel.setWordtoreplace(wordtoreplace);
            seoSettingsModel.setWordtoremove(wordtoremove);
            seoSettingsModel.setSynonym(synonym);

            seoSettingsDTO.entityToService(seoSettingsModel);

            seoSettingsDAO.create(seoSettingsModel);

//            if (userDTO.getId() != null && !userDTO.getId().equals("")) {
//                seoSettingsDAO.create(seoSettingsModel);
//            }
            model.addAttribute("seoset", seoSettingsModel);
            model.addAttribute("who", userDTO);
            return "seosettings";
        }

        //if settings are NOT OK
        if (ftpHost == null || ftpHost.length() == 0)
            seoset.setFtphost("FTP.host");
        if (ftpLogin == null || ftpLogin.length() == 0)
            seoset.setFtplogin("FTPUserName");
        if (ftpPwd == null || ftpPwd.length() == 0)
            seoset.setFtppwd("FTPUserPassword");
        if (!(ftpPort >= 0))
            seoset.setFtpport(21);

        model.addAttribute("seoset", seoset);
        //model.addAttribute("dto", seoSettingsModel);
        model.addAttribute("who", userDTO);
        return "seosettings";
    }
}