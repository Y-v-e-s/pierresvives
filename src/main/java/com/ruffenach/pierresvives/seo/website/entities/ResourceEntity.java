package com.ruffenach.pierresvives.seo.website.entities;

import com.ruffenach.pierresvives.dto.ResourceDTO;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Class representing a resource.
 * It is a pojo that benefits from a few additional methods including a toString() optimized for the web.
 */
public class ResourceEntity {

    public ResourceEntity() {
    }

    @Id
    private String id;

    private long rnano = System.nanoTime();
    private String rtitle = "";
    private String rdescription = "";
    private String rtext = "";

    private String rpageurl = "";

    private String userid = "";           //get user id, if end session & userid empty then delete all db & dto
    public String getUserid() {
        return userid;
    }
    public void setUserid(String userid) {
        this.userid = userid;
    }

    private String username;
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.rnano = System.nanoTime();   //logiquement...
        this.id = id;
    }

    public long getRnano() {
        return rnano;
    }
    public void setRnano(long rnano) {
        this.rnano = rnano;
    }

    public String getRtitle() {
        return rtitle;
    }
    public void setRtitle(String rtitle) {
        this.rnano = System.nanoTime();   //création effective
        this.rtitle = rtitle;
    }

    public String getRdescription() {
        return rdescription;
    }
    public void setRdescription(String rdescription) {
        this.rdescription = rdescription;
    }

    public String getRtext() {
        return rtext;
    }
    public void setRtext(String rtext) {
        this.rnano = System.nanoTime();   //opération la plus longue, plus pertinent ?
        this.rtext = rtext;
    }

    public String getRpageurl() {
        return rpageurl;
    }
    public void setRpageurl(String rpageurl) {
        this.rpageurl = rpageurl;
    }

    @Override
    public String toString() {
        return ">_ResourceModel <br>" +
                "<br> id =" + id +
                "<br> nano seconds =" + rnano +
                "<br> user id =" + userid +
                "<br> title =" + rtitle +
                "<br> description =" + rdescription +
                "<br> text =" + rtext +
                "<br> page url =" + rpageurl +
                "<br><br> ";
    }

    public String toStringMini() {
        return ">_ResourceModel ________<br>" +
                "<br> >_nano seconds ___" + rnano +
                "<br> >_title __________" + rtitle +
                "<br> >_description ____" + rdescription +
                "<br> >_text ___________" + rtext +
                "<br> >_page url _______" + rpageurl +
                "<br>>__________________<br> ";
    }

    public void copyDTOToModel(ResourceDTO rr) {
        this.id = rr.getId();
        this.rnano = rr.getRnano();
        this.rtitle = rr.getRtitle();
        this.rdescription = rr.getRdescription();
        this.rtext = rr.getRtext();
        this.rpageurl = rr.getRpageurl();
        this.userid = rr.getUserid();
    }

    public void resourceInit() {
    }
}