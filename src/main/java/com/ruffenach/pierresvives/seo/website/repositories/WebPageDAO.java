package com.ruffenach.pierresvives.seo.website.repositories;

import com.ruffenach.pierresvives.dto.SEOSettingsSessionService;
import com.ruffenach.pierresvives.seo.ftp.FTPConnection;
import com.ruffenach.pierresvives.seo.website.entities.WebPageEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
/**
 * Persistent manager for accessing and manipulating web pages.
 * Repository access class (a repository is an interface grouping a set of tools to ensure data persistence,
 * to interact with the database).
 *
 * This class includes the following methods:
 * create() to save web page,
 * deleteAllNewWebPageInDB() which deletes all web pages in db,
 * getAll() which returns a list of all web pages,
 * getSize() which returns the size of the list of all web pages,
 * publish() which publishes newly created web pages to the user's site via FTP.
 *
 * The repository associated with MongoDB does not offer an update :
 * so if the ids match, the previous record is overwritten.
 *
 * Note: as this application is intended to be used by multiple users,
 * the solution adopted to partition, individualize, the data, in particular within the db,
 * is the addition of a filter represented by the username to all entities and for all processing.
 */
@Component
public class WebPageDAO {

    @Autowired
    WebPageRepository repository;

    @Autowired
    FTPConnection ftpConnection;

    @Autowired
    SEOSettingsSessionService seoSettingsDTO;

    public String create(WebPageEntity w) {
        repository.save(w);
        return w.getId();
    }

    public List<WebPageEntity> getAll(String username) {
        return repository.findAllByUsername(username);
    }

    public int getSize(String username) {
        return getAll(username).size();
    }

    public void deleteAllNewWebPageInDB() {
        repository.deleteAll();
    }

    public void publish(String username) {
        ftpConnection.FTPConnect(seoSettingsDTO.getFtphost(), seoSettingsDTO.getFtplogin(), seoSettingsDTO.getFtppwd(),
                seoSettingsDTO.getFtpport());
        List<WebPageEntity> l = getAll(username);

        for (int i = 0; i < getSize(username); i++) {

            try {
                ftpConnection.upld(l.get(i).getCodesource(), l.get(i).getUrl());
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    ftpConnection.checkConnectionWithOneRetry(seoSettingsDTO.getFtphost(),
                            seoSettingsDTO.getFtplogin(),
                            seoSettingsDTO.getFtppwd(),
                            seoSettingsDTO.getFtpport());
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        ftpConnection.FTPDisconnect();
    }
}