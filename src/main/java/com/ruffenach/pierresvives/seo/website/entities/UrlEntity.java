package com.ruffenach.pierresvives.seo.website.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
/**
 * Class representing a URL.
 */
public class UrlEntity {

    @Id
    private String id;
    private String url="";
    private long nano;

    private String username;
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    public UrlEntity() {
        this.nano=System.nanoTime();
    }

    public UrlEntity(String url, String username) {
        this.url = url;
        this.nano=System.nanoTime();
        this.username=username;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }
    public void setUrl(String url) { this.url = url; }

    public long getNano() {
        return nano;
    }
    public void setNano(long nano) {
        this.nano = nano;
    }
}
