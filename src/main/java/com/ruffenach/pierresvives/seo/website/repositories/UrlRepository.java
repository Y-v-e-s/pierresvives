package com.ruffenach.pierresvives.seo.website.repositories;

import com.ruffenach.pierresvives.seo.website.entities.UrlEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

/**
 * Interface that offers the necessary tools to use DB and allow persistence of URLs.
 * The PagingAndSortingRepository interface, an extension of CrudRepository,
 * provides tools for sorting the data used in this project, and one method in particular:
 * Iterable findAll(Sort sort): returns all entities sorted by the given options.
 */
public interface UrlRepository extends MongoRepository<UrlEntity, String>
    //PagingAndSortingRepository<UrlEntity, String>
{
    Iterable<UrlEntity> findAllByUsername(String username);
    void deleteAllByUsername(String username);
    List<UrlEntity> findAllByUsernameOrderByNanoDesc(String username);
}
