package com.ruffenach.pierresvives.seo.website.repositories;

import com.ruffenach.pierresvives.seo.website.entities.ResourceEntity;
import com.ruffenach.pierresvives.seo.website.entities.UrlEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

/**
 * Interface that offers the necessary tools to use DB and allow persistence of resources.
 * The PagingAndSortingRepository interface, an extension of CrudRepository,
 * provides tools for sorting the data used in this project, and one method in particular:
 * Iterable findAll(Sort sort): returns all entities sorted by the given options.
 */
public interface ResourceRepository extends MongoRepository<ResourceEntity, String>
    //PagingAndSortingRepository<ResourceEntity, String>
{
    List<ResourceEntity> findAllByUsernameOrderByRnanoDesc(String username);
    List<ResourceEntity> findAllByUsernameOrderByRnanoAsc(String username);
    List<ResourceEntity> findAllByUsername(String username);
    void deleteAllByUsername(String username);
}
