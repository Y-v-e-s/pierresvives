package com.ruffenach.pierresvives.seo.website.repositories;

import com.ruffenach.pierresvives.seo.website.entities.ResourceEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
/**
 * Persistent manager for accessing and manipulating resources.
 * Repository access class (a repository is an interface grouping a set of tools to ensure data persistence,
 * to interact with the database).
 *
 * This class includes the following methods:
 * create() to save resource,
 * getAllSortedDesc() which returns a list of descending sorted resources,
 * getAllSortedAsc() which returns a list of ascending sorted resources,
 * getResourceAndDelete() which reads a saved resource and returns it after deleting it,
 * deleteResource() which deletes one resource,
 * deleteAllResourceInDB() which deletes all resources in db,
 * getAll() which returns a list of all resources,
 * getSize() which returns the size of the list of all resources.
 *
 * The repository associated with MongoDB does not offer an update :
 * so if the ids match, the previous record is overwritten.
 */
@Repository
public class ResourceDAO {
    @Autowired
    ResourceRepository repository;

    public String create(ResourceEntity resourceModel) {
        repository.save(resourceModel);
        System.out.println("ResourceDAO : resourceModel.getId() "+resourceModel.getId());
        return resourceModel.getId();
    }

    public List<ResourceEntity> getAllSortedDesc(String username) {
        //Sort sort = Sort.by("rnano").descending();
        List<ResourceEntity> l = new ArrayList<>();
        //repository.findAll(sort).forEach(l::add);
        l=repository.findAllByUsernameOrderByRnanoDesc(username);
        return l;
    }

    public List<ResourceEntity> getAllSortedAsc(String username) {
        //Sort sort = Sort.by("rnano").ascending();
        List<ResourceEntity> l = new ArrayList<>();
        //repository.findAll(sort).forEach(l::add);
        l=repository.findAllByUsernameOrderByRnanoAsc(username);
        return l;
    }

    //si en série................................
    public ResourceEntity getResourceAndDelete(String username){
        String s = "";
        Iterator<ResourceEntity> iterator = getAllSortedAsc(username).iterator();
        ResourceEntity resource = null;
        if(iterator.hasNext()) {
            resource = iterator.next();
        }
        deleteResource(resource);
        return resource;
    }

    public void deleteResource(ResourceEntity resource){
        repository.delete(resource);
    }
    public void deleteAllResourceInDB(String username){
        repository.deleteAllByUsername(username);
    }

    public List<ResourceEntity> getAll(String username){
        List<ResourceEntity> l=new ArrayList<>();
        l=repository.findAllByUsername(username);
        //System.out.println("ResourceDAO : get all "+l.size());
        return l;
    }

    public int getSize(String username) {
        int i=getAll(username).size();
        //System.out.println("ResourceDAO : size "+i);
        return i;
    }
}