package com.ruffenach.pierresvives.seo.website.repositories;

import com.ruffenach.pierresvives.dto.WebPageDTO;
import com.ruffenach.pierresvives.seo.website.entities.ResourceEntity;
import com.ruffenach.pierresvives.seo.website.entities.WebPageEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Interface that offers the necessary tools to use MongoDB and allow persistence of collected web pages.
 * Interface that extends MongoRepository<T,ID> and provides a set of tools for data manipulation.
 * The MongoRepository<T,ID> interface which extends CrudRepository <T,ID>,
 * PagingAndSortingRepository <T,ID>,
 * QueryByExampleExecutor <T>,
 * Repository <T,ID>
 *     is implemented in this project.
 */
public interface WebPageRepository extends MongoRepository<WebPageEntity, String> {
    List<WebPageEntity> findAllByUsername(String username);
    void deleteAllByUsername(String username);
}
