package com.ruffenach.pierresvives.seo.website.repositories;

import com.ruffenach.pierresvives.seo.website.entities.UrlEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
/**
 * Persistent manager for accessing and manipulating URLs.
 * Repository access class (a repository is an interface grouping a set of tools to ensure data persistence,
 * to interact with the database).
 *
 * This class includes the following methods:
 * create() to save resource,
 * getAllSortedDesc() which returns a list of descending sorted URLs,
 * getUrlAndDelete() which reads a saved URL and returns it after deleting it,
 * deleteDB() which deletes one URL,
 * deleteAllURLInDB() which deletes all URLs in DB,
 * getAll() which returns a list of all URLs,
 * getSize() which returns the size of the list of all URLs.
 *
 * The repository associated with MongoDB does not offer an update :
 * so if the ids match, the previous record is overwritten.
 *
 * Note: as this application is intended to be used by multiple users,
 * the solution adopted to partition, individualize, the data, in particular within the db,
 * is the addition of a filter represented by the username to all entities and for all processing.
 */
@Component
public class UrlDAO {

    @Autowired
    private UrlRepository websiteRepository;

    public String create(String url, String username){
        UrlEntity u;
        u = new UrlEntity(url, username);
        websiteRepository.save(u);
        return u.getId();
    }

    public List<UrlEntity> getAll(String username){
        List<UrlEntity> l=new ArrayList<>();
        websiteRepository.findAllByUsername(username).forEach(l::add);
        return l;
    }

    //on compte toutes les fiches de la db (ou colonne) des urls
    public int getSize(String username) {
        int i=getAll(username).size();
        return i;
    }

    public void deleteDB(UrlEntity u){
        websiteRepository.delete(u);
    }

    public void deleteByIdDB(String id){
        websiteRepository.deleteById(id);
    }

    public void deleteAllURLInDB(String username){
        websiteRepository.deleteAllByUsername(username);
    }

    //on download, lit, on retourne l'url (string) et on supprime la fiche
    public String getUrlAndDelete(String username){
        String s = "";
        Iterator<UrlEntity> iterator = websiteRepository.findAllByUsername(username).iterator();
        if(iterator.hasNext()) {
            UrlEntity u = iterator.next();
            s = u.getUrl();
            deleteDB(u);
        }
        return s;
    }

    public List<UrlEntity> getAllSortedDesc(String username){
        List<UrlEntity> l=new ArrayList<>();
        l=websiteRepository.findAllByUsernameOrderByNanoDesc(username);
        //websiteRepository.findAllByUsernameOrderByNanoDesc(username).forEach(l::add);
        return l;
    }
}