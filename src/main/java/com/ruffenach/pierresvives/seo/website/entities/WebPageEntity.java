package com.ruffenach.pierresvives.seo.website.entities;

import org.springframework.data.annotation.Id;

/**
 * Class representing a pweb page.
 */
public class WebPageEntity {

    @Id
    private String id;

    private String url = ""; //something.html
    private String codesource = "";

    private String username;
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    public WebPageEntity() {
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }

    public String getCodesource() {
        return codesource;
    }
    public void setCodesource(String codesource) {
        this.codesource = codesource;
    }

    @Override
    public String toString() {
        return ">_Web page" +
                "<br>>_<br>" +
                ">_id________" + id +
                "<br>" +
                ">_url_______" + url +
                "<br>" +
                ">_codesource" +
                "<br><textarea disabled>" + codesource +
                "</textarea><br>" +
                "<br>________";
    }
}
