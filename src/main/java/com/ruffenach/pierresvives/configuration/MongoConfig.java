package com.ruffenach.pierresvives.configuration;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;

import java.util.Collection;
import java.util.Collections;

/**
 * base class extension for MongoDB configuration, AbstractMongoConfiguration
 * MongoTemplate follows the standard Spring template pattern
 * and provides a basic out-of-the-box API to the underlying persistence engine
 * MongoTemplate is defined in AbstractMongoClientConfiguration
 */
@Configuration
public class MongoConfig extends AbstractMongoClientConfiguration {

    @Override
    protected String getDatabaseName() {
        return "pierresvives";
    }

    @Override
    public MongoClient mongoClient() {
        ConnectionString connectionString = new ConnectionString("mongodb://localhost:27017/pierresvives");
        MongoClientSettings mongoClientSettings = MongoClientSettings.builder()
                .applyConnectionString(connectionString)
                .build();

        return MongoClients.create(mongoClientSettings);
    }

//    @Override
//    public Collection getMappingBasePackages() {
//        return Collections.singleton("com.ruffenach");
//    }
}