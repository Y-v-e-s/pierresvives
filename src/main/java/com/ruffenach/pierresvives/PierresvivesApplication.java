package com.ruffenach.pierresvives;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

/**
 * Application class.
 * @SpringBootApplication annotation implements these three features:
 *		@EnableAutoConfiguration enables Spring Boot's auto-configuration mechanism,
 *		@ComponentScan enable @Component scan on the package where the app is located,
 *		@Configuration allows additional beans or configuration classes.
 *
 * if :
 * org.springframework.security.web.firewall.RequestRejectedException:
 * The request was rejected because the URL contained a potentially malicious String ";"
 * see :
 * https://stackoverflow.com/questions/52566368/spring-boot-requestrejectedexception-the-request-was-rejected-because-the-url
 * https://www.baeldung.com/spring-security-request-rejected-exception
 */
@SpringBootApplication
public class PierresvivesApplication {

	public static void main(String[] args) {
		SpringApplication.run(PierresvivesApplication.class, args);
	}
}
