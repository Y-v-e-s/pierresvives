package com.ruffenach.pierresvives.security;

import com.ruffenach.pierresvives.authentication.repositories.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * Class which configure HttpSecurity by using the WebSecurityConfigurerAdapter.
 *
 * Note:
 * In Spring Security 5.7, WebSecurityConfigurerAdapter became deprecated,
 * Spring pushing users to move towards a component-based security configuration :
 * https://spring.io/blog/2022/02/21/spring-security-without-the-websecurityconfigureradapter
 */
@Component
@Configuration
@EnableConfigurationProperties
public class SecurConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    UserService userDetailsService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/",
                        "/index",
                        "/registration",
//                        "/seobot",
//                        "/seosettings",
//                        "/seosettingsnext",
//                        "/image",
//                        "/imagenext",
                        "/red",
                        "/redworktoindex",
                        "/seobotworking",
                        "/deleteallurlindb",
                        "/stopharvesting",
                        "/showallurlindb",
                        "/showallnewpageindb",
                        "/showallresourceindb",
                        "/css/**",
                        "/images/**",
                        "/js/**"
                ).permitAll()
                .and()
                .formLogin()
                    .loginPage("/login")
                    .permitAll()
                    .successForwardUrl("/index")
                    .defaultSuccessUrl("/index")
                    .failureUrl("/login")
                .and()
                .logout()
                    .permitAll()
                    .logoutSuccessUrl("/index")
                .and()
                .csrf().disable()
                .authorizeRequests().anyRequest().authenticated()
                .and().httpBasic()
                .and().sessionManagement().disable();


//        .and().logout()
//        .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
//        .logoutSuccessUrl("/").and().exceptionHandling();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * authentication customization
     * as a parameter the new authentication method
     */
    @Override
    public void configure(AuthenticationManagerBuilder builder) throws Exception {
        builder.userDetailsService(userDetailsService);
    }

    /**
     * check the validity of a password
     */
    public String getDBPwd(String s) {
        return passwordEncoder().encode(s);
    }
}