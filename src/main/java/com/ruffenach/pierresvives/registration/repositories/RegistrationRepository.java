package com.ruffenach.pierresvives.registration.repositories;

import com.ruffenach.pierresvives.user.entities.UserEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Interface that offers the necessary tools to use MongoDB and allow persistence of registration data.
 * Interface that extends MongoRepository<T,ID> and provides a set of tools for data manipulation.
 * The MongoRepository<T,ID> interface which extends CrudRepository <T,ID>,
 * PagingAndSortingRepository <T,ID>,
 * QueryByExampleExecutor <T>,
 * Repository <T,ID>
 *     is implemented in this project.
 */
public interface RegistrationRepository extends MongoRepository<UserEntity, String> {
    UserEntity findByUsername(String username);
    boolean existsByUsername(String username);
    void deleteByUsername(String username);
}