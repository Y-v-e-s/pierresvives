package com.ruffenach.pierresvives.registration.repositories;
import com.ruffenach.pierresvives.user.entities.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Optional;
/**
 * Registration persistent data access class.
 * This class includes the following methods:
 * save() to save registration data,
 * getById() which returns a user from the Id,
 * getByUsername() which returns a user from the username,
 * usernameAlreadyExists() which returns a boolean.
 *
 * The repository associated with MongoDB does not offer an update :
 * so if the ids match, the previous record is overwritten.
 */
@Service
public class RegistrationService {

    @Autowired
    private RegistrationRepository repository;

    public String save(String username,
                       String password,
                       String firstname,
                       String lastname,
                       String email) {
        UserEntity user = new UserEntity(username, password,firstname,lastname,email);
        repository.save(user);
        return user.getId();
    }

    public String update(String id,
                         String username,
                         String password,
                         String firstname,
                         String lastname,
                         String email) {
        UserEntity s = new UserEntity(id, username, password,firstname,lastname,email);
        repository.save(s);
        return s.getId();
    }

    public Optional<UserEntity> getById(String id) {
        return repository.findById(id);
    }

    public UserEntity getByUsername(String username) {
        return repository.findByUsername(username);
    }

    public void deleteById(String id) {
        repository.deleteById(id);
    }
    public void deleteByUsername(String username) {
        repository.deleteByUsername(username);
    }

    public String getPwdByEmail(String username) {
        return repository.findByUsername(username).getPassword();
    }

    public boolean usernameAlreadyExists(String username) {
        return repository.existsByUsername(username);
    }
}