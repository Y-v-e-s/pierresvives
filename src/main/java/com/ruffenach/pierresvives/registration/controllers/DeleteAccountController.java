package com.ruffenach.pierresvives.registration.controllers;

import com.ruffenach.pierresvives.registration.repositories.RegistrationService;
import com.ruffenach.pierresvives.security.SecurConfig;
import com.ruffenach.pierresvives.seo.entities.SEOSettingsEntity;
import com.ruffenach.pierresvives.seo.repositories.SEOSettingsPersistentService;
import com.ruffenach.pierresvives.user.entities.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * Class to delete user account
 */
@Controller
public class DeleteAccountController {

    @Autowired
    SecurConfig secureConfig;

    @Autowired
    RegistrationService registrationDAO;

    @Autowired
    SEOSettingsPersistentService seoSettingsDAO;

    SEOSettingsEntity seoSettingsModel = new SEOSettingsEntity();

    @GetMapping("/deleteaccount")
    public String registrationForm(Model model, @ModelAttribute("deleteaccount") UserEntity ue) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();

        if (username!=null) {UserEntity userModel = registrationDAO.getByUsername(username);
        model.addAttribute("who", userModel);}
        return "deleteaccount";
    }

    @PostMapping("/deleteaccount")
    public String registrationSubmit(@ModelAttribute("deleteaccount") UserEntity ue) {

        String username = "";
        String password = "";

        username = ue.getUsername();
        password = ue.getPassword();

        if (!registrationDAO.usernameAlreadyExists(username))
            return "deleteaccount";
        UserEntity userEntity = null;
        userEntity = registrationDAO.getByUsername(username);

        if (userEntity != null && secureConfig.passwordEncoder().matches(password,userEntity.getPassword()))
            registrationDAO.deleteByUsername(username);

        return "index";
    }
}
