package com.ruffenach.pierresvives.registration.controllers;

import com.ruffenach.pierresvives.security.SecurConfig;
import com.ruffenach.pierresvives.seo.entities.SEOSettingsEntity;
import com.ruffenach.pierresvives.seo.repositories.SEOSettingsPersistentService;
import com.ruffenach.pierresvives.user.entities.UserEntity;
import com.ruffenach.pierresvives.registration.repositories.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.view.RedirectView;

/**
 * Class that manages the registration page. Via the method annotated with @PostMapping,
 * it notably manages the data provided by the registration form,
 * makes it possible to verify the uniqueness of the username
 * and allows the saving of registration data.
 *
 * @Controller indicates that this annotated class is a web controller.
 * It's a specialization of @Component
 * allowing for implementation classes to be autodetected through classpath scanning.
 * It is used in combination with annotated handler methods (@GetMapping(...), PostMapping(...)).
 */
@Controller
public class RegistrationController {

    @Autowired
    SecurConfig secureConfig;

    @Autowired
    RegistrationService registrationDAO;

    @Autowired
    SEOSettingsPersistentService seoSettingsDAO;

    @Value("${index.help7}")
    private String indexhelp7;
    @Value("${index.help8}")
    private String indexhelp8;
    @Value("${index.help8.1}")
    private String indexhelp8p1;
    @Value("${index.help9}")
    private String indexhelp9;
    @Value("${index.help10}")
    private String indexhelp10;
    @Value("${index.help11}")
    private String indexhelp11;

    SEOSettingsEntity seoSettingsModel=new SEOSettingsEntity();

    @GetMapping("/registration")
    public String registrationForm(@ModelAttribute("registration") UserEntity registration, Model model){

        model.addAttribute("indexhelp7", indexhelp7);
        model.addAttribute("indexhelp9", indexhelp9);
        model.addAttribute("indexhelp8", indexhelp8);
        model.addAttribute("indexhelp8p1", indexhelp8p1);
        model.addAttribute("indexhelp10", indexhelp10);
        model.addAttribute("indexhelp11", indexhelp11);

        return "registration";
    }

    @PostMapping("/registration")
    public String registrationSubmit(@ModelAttribute("registration") UserEntity registration){
        String username = "";
        String password = "";
        String firstname = "";
        String lastname = "";
        String email = "";

        username = registration.getUsername();
        password = registration.getPassword();
        firstname = registration.getFirstname();
        lastname = registration.getLastname();
        email = registration.getEmail();

        String pwdHash = secureConfig.getDBPwd(password);
        if(registrationDAO.usernameAlreadyExists(username))
            return"registration";

        String id = registrationDAO.save(username,pwdHash,firstname,lastname,email);

        seoSettingsModel.setId(id);
        seoSettingsModel.setUsername(username);
        seoSettingsDAO.create(seoSettingsModel);

        /*pwdHash = secureConfig.getDBPwd("nouveau mot de passe");
        id = dataAccess.update(id,"nouveauz",pwdHash);
        System.out.println("........................................"+id);
        System.out.println("getbyid : "+dataAccess.getById(id).toString());
        dataAccess.deleteById(id);
        System.out.println("getbyid : "+dataAccess.getById(id).toString());*/

        return "login";
    }
}
