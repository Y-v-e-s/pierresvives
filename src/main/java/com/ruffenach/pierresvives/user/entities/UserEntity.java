package com.ruffenach.pierresvives.user.entities;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
/**
 * Class representing a user
 */
public class UserEntity {
    @Id
    public String id;

    public String username="";
    public String password="";

    private String firstname="";
    private String lastname="";
    private String email="";

    public UserEntity() {
    }
    public UserEntity(String id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }
    public UserEntity(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public UserEntity(String username,
                      String password,
                      String firstname,
                      String lastname,
                      String email) {
        this.username = username;
        this.password = password;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
    }

    public UserEntity(String id,
                      String username,
                      String password,
                      String firstname,
                      String lastname,
                      String email) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
    }

    public void setId(ObjectId _id) {
        this.id = id;
    }
    public String getId() {
        return this.id;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    public String getUsername() {
        return username;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public String getPassword() {
        return password;
    }

    public String getFirstname() {
        return firstname;
    }
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Web-optimized toString method.
     */
    @Override
    public String toString() {
        return "<br> Users " +
                "<br> id.........." + id +
                "<br> username...." + username +
                "<br> password...." + password +
                "<br> firstname..." + firstname +
                "<br> lastname...." + lastname +
                "<br> email......." + email +
                "<br>";
    }
}