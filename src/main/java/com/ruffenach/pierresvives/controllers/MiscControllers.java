package com.ruffenach.pierresvives.controllers;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * This web application is based on Spring MVC.
 * We configure Spring MVC and configure view controllers to display these pages.
 * This class partially configures Spring MVC.
 * This is a solution to display any blank controller page.
 * The addViewControllers() method overrides the same name method in WebMvcConfigurer and adds view controllers.
 */
@Configuration
public class MiscControllers implements WebMvcConfigurer {

    public void addViewControllers(ViewControllerRegistry registry) {
//        registry.addViewController("/index").setViewName("index");
        registry.addViewController("/").setViewName("index");
        registry.addViewController("/login").setViewName("login");
        registry.addViewController("/seobotworking").setViewName("seobotworking");
        registry.addViewController("/showallurlindb").setViewName("showallurlindb");
        registry.addViewController("/showallnewpageindb").setViewName("showallnewpageindb");
        registry.addViewController("/logout").setViewName("logout");
    }
}