package com.ruffenach.pierresvives.dto;

import com.ruffenach.pierresvives.seo.entities.SEOSettingsEntity;
import com.ruffenach.pierresvives.user.entities.UserEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

/**
 * Class to define the current user, the "principal"
 * Contains all ephemeral or permanent data concerning the principal
 * These are Session-scoped beans.
 * In addition to the bean-specific methods, the beans also come with a toString() method intended for the web.
 *
 * The first draft of the project used DTOs whose scope was the session.
 * These DTOs have been phased out.
 */
@Component
@SessionScope
public class UserSessionService {

    private String id = "";
    private String user = "";
    private String pwd = "";

    private String firstname = "";
    private String lastname = "";
    private String email = "";

    public void setId(String id) {
        this.id = id;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getId() {
        return id;
    }

    public String getUser() {
        return user;
    }

    public String getPwd() {
        return pwd;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserSessionService() {
    }

    /**
     * A convenient web oriented toString method.
     */
    @Override
    public String toString() {
        return ">_User <br>" +
                "<br> id________" + id +
                "<br> username__" + user +
                "<br> firstname_" + firstname +
                "<br> lastname__" + lastname +
                "<br> email_____" + email +
                "<br> __________" +
                "<br> ";
    }

    /**
     * Method to copy entity attributes to DTO
     */
    public void entityToDTO(UserEntity userEntity) {
        this.setId(userEntity.getId());
        this.setUser(userEntity.getUsername());
        this.setPwd(userEntity.getPassword());
        this.setFirstname(userEntity.getFirstname());
        this.setLastname(userEntity.getLastname());
        this.setEmail(userEntity.getEmail());
    }
}