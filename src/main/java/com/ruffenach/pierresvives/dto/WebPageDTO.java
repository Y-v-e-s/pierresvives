package com.ruffenach.pierresvives.dto;

import org.springframework.context.annotation.Scope;
import org.springframework.data.annotation.Id;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

/**
 * Class similar to a DTO whose object is the manipulation of web pages.
 * This class has all the characteristics of a bean with a few additional methods
 * including an init() method to reset all data
 * and a toString() method to facilitate displaying data on the web.
 * The scope of this bean is the Session.
 */
@Component
@SessionScope
public class WebPageDTO {

    @Id
    private String id;

    private String url = ""; //something.html
    private String codesource = "";

    private String username;
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    private String userid = "";           //get user id, if end session & userid empty then delete all db & dto
    public String getUserid() {
        return userid;
    }
    public void setUserid(String userid) {
        this.userid = userid;
    }

    public WebPageDTO() {
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }

    public String getCodesource() {
        return codesource;
    }
    public void setCodesource(String codesource) {
        this.codesource = codesource;
    }

    public void init(){
        id="";
        url = "";
        codesource = "";
        userid = "";
    }

    @Override
    public String toString() {
        return ">__WebPageDTO" +
                "<br>>_<br>" +
                ">_id________" + id +
                "<br>" +
                ">_url_______" + url +
                "<br>" +
                ">_codesource" +
                "<br><textarea disabled>" + codesource +
                "</textarea><br>" +
                ">_userid____" + userid +
                "<br>________";
    }
}