package com.ruffenach.pierresvives.dto;

import com.ruffenach.pierresvives.seo.website.entities.ResourceEntity;
import org.springframework.data.annotation.Id;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

/**
 * Class that treats collected resources as DTOs. These DTOs are beans whose scope is the Session.
 * They are annotated by @Component that allows Spring to automatically detect these beans.
 * These beans have, in addition to the methods inherent to beans,
 * a web-oriented toString method, an init() method and a copyModelToDTO() method.
 * Resources collected include a page's title, description, and address.
 */
@Component
@SessionScope
public class ResourceDTO {

    public ResourceDTO() {
    }

    @Id
    private String id;
    private long rnano = System.nanoTime();
    private String rtitle = "";
    private String rdescription = "";
    private String rtext = "";

    private String rpageurl = "";

    private String userid = "";           //get user id, if end session & userid empty then delete all db & dto

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.rnano = System.nanoTime();   //logiquement...
        this.id = id;
    }

    public long getRnano() {
        return rnano;
    }

    public void setRnano(long rnano) {
        this.rnano = rnano;
    }

    public String getRtitle() {
        return rtitle;
    }

    public void setRtitle(String rtitle) {
        this.rnano = System.nanoTime();   //création effective
        this.rtitle = rtitle;
    }

    public String getRdescription() {
        return rdescription;
    }

    public void setRdescription(String rdescription) {
        this.rdescription = rdescription;
    }

    public String getRtext() {
        return rtext;
    }

    public void setRtext(String rtext) {
        this.rnano = System.nanoTime();   //opération la plus longue, plus pertinent ?
        this.rtext = rtext;
    }

    public String getRpageurl() {
        return rpageurl;
    }

    public void setRpageurl(String rpageurl) {
        this.rpageurl = rpageurl;
    }

    /**
     * toString() method optimized for the web, with, for example, line breaks <br>
     */
    @Override
    public String toString() {

        return ">_ResourceDTO ... <br>" +
                "<br> id ... " + id +
                "<br> nano seconds ... " + rnano +
                "<br> userid ... " + userid +
                "<br> title ... " + rtitle +
                "<br> description ... " + rdescription +
                "<br> text ... " + rtext +
                "<br> page url ... " + rpageurl +
                "<br><br> ";
    }

    /**
     * method to reset DTO data
     */
    public void init() {
        this.id = "";
        this.rnano = System.nanoTime();
        this.rtitle = "";
        this.rdescription = "";
        this.rtext = "";
        this.rpageurl = "";
    }

    /**
     * method to copy data from model, or entity, to DTO
     */
    public void copyModelToDTO(ResourceEntity rr){
        this.id= rr.getId();
        this.rnano = System.nanoTime();
        this.rtitle = rr.getRtitle();
        this.rdescription = rr.getRdescription();
        this.rtext = rr.getRtext();
        this.rpageurl = rr.getRpageurl();
        this.userid = rr.getUserid();
    }
}