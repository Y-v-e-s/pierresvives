package com.ruffenach.pierresvives.dto;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

/**
 * Retrieve authenticated user details with Spring Security.
 * Get the authenticated user in a bean :
 * get the current authenticated user, the "principal", via a static call to SecurityContextHolder
 */
@Component
public class AuthenticatedName {
    public String getAuthenticatedName(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            String currentUserName = authentication.getName();
            return currentUserName;
        }
        else return null;
    }
}
