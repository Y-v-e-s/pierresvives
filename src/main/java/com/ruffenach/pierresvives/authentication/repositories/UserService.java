package com.ruffenach.pierresvives.authentication.repositories;

import com.ruffenach.pierresvives.user.entities.UserEntity;
import com.ruffenach.pierresvives.registration.repositories.RegistrationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

/**
 * Class that replaces the default loadUserByUsername method by a method that looks for the User in the DB.
 * ...implements UserDetailsService { ... } :
 * create a class to search and authenticate the user,
 * this will allow us to replace the original method by a method that searches for the user in the DB,
 * this is a user data access class
 *
 * @Component is an annotation that allows Spring to automatically detect beans.
 * Spring will scan the application for classes annotated with @Component,
 * instantiate them,
 * inject the specified dependencies into them,
 * and inject them elsewhere if needed.
 */
@Component
public class UserService implements UserDetailsService {
    @Autowired
    private RegistrationRepository repository;
    /**
     * the default loadUserByUsername method is replaced by a method that looks for the User in the DB,
     * it returns a User(username, password, authorities) using repository.findByUsername(username);
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity user=null;
        if(username!=null && !username.equals("")) user = repository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }
        List<SimpleGrantedAuthority> authorities =
                Arrays.asList(new SimpleGrantedAuthority("user"));
        return new User(user.getUsername(), user.getPassword(), authorities);
    }
}