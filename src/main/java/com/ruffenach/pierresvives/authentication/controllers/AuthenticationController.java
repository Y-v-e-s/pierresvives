package com.ruffenach.pierresvives.authentication.controllers;

import com.ruffenach.pierresvives.dto.AuthenticatedName;
import com.ruffenach.pierresvives.dto.SEOSettingsSessionService;
import com.ruffenach.pierresvives.dto.UserSessionService;
import com.ruffenach.pierresvives.registration.repositories.RegistrationService;
import com.ruffenach.pierresvives.seo.repositories.SEOSettingsPersistentService;
import com.ruffenach.pierresvives.user.entities.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * Controller class for "/" , "/index" and "/login"
 * Spring controller class for get on "/" and "/index"
 * This controller declares the indexGet method which handles requests for path / and for path /index.
 * Controller indicates that this annotated class is a web controller.
 * It's a specialization of @Component
 * allowing for implementation classes to be autodetected through classpath scanning.
 * It is used in combination with annotated handler methods (@GetMapping(...)...).
 */
@Controller
public class AuthenticationController {

    @Autowired
    RegistrationService registrationDAO;

    @Autowired
    UserSessionService userDTO;

    @Autowired
    AuthenticatedName authenticatedName;

    @Autowired
    SEOSettingsSessionService seoSettingsDTO;

    @Autowired
    SEOSettingsPersistentService seoSettingsDAO;

    @Value("${useraccountdetails}")
    private String useraccountdetails;
    @Value("${index.property}")
    private String indexproperty;
    @Value("${index.management}")
    private String indexmanagement;
    @Value("${index.system}")
    private String indexsystem;
    @Value("${index.help1}")
    private String indexhelp1;
    @Value("${index.help2}")
    private String indexhelp2;
    @Value("${index.help3}")
    private String indexhelp3;
    @Value("${index.help4}")
    private String indexhelp4;
    @Value("${index.seo}")
    private String indexseo;
    @Value("${index.secrets}")
    private String indexsecrets;
    @Value("${index.improve}")
    private String indeximprove;
    @Value("${index.practically}")
    private String indexpractically;
    @Value("${index.fulfill}")
    private String indexfulfill;
    @Value("${index.help5}")
    private String indexhelp5;
    @Value("${index.help6}")
    private String indexhelp6;
    @Value("${index.help12}")
    private String indexhelp12;
    @Value("${index.console}")
    private String indexconsole;
    @Value("${index.search}")
    private String indexsearch;
    @Value("${index.seo2}")
    private String indexseo2;
    @Value("${index.try}")
    private String indextry;
    @Value("${index.gives}")
    private String indexgives;
    @Value("${index.login}")
    private String indexlogin;
    @Value("${index.provides}")
    private String indexprovides;
    @Value("${index.register}")
    private String indexregister;
    @Value("${index.all}")
    private String indexall;
    @Value("${index.home}")
    private String indexhome;
    @Value("${index.enter}")
    private String indexenter;
    @Value("${index.seosettings}")
    private String indexseosettings;
    @Value("${index.ifyou}")
    private String indexifyou;
    @Value("${index.saveimages}")
    private String indexsaveimages;
    @Value("${index.drop}")
    private String indexdrop;
    @Value("${index.and}")
    private String indexand;
    @Value("${index.enjoy}")
    private String indexenjoy;
    @Value("${index.searchandpublish}")
    private String indexsearchandpublish;
    @Value("${index.contact}")
    private String indexcontact;
    @Value("${index.feedback}")
    private String indexfeedback;
    @Value("${index.swing}")
    private String indexswing;
    @Value("${index.or}")
    private String indexor;
    @Value("${index.send}")
    private String indexsend;
    @Value("${index.tothetop}")
    private String indextothetop;


    /**
     * The following method reacts to GET requests on the "/" or "/'index" paths and allows the user's attributes
     * to be displayed on the "index" page.
     * The first draft of the project used DTOs whose scope was the session.
     * These DTOs have been phased out.
     */
    @GetMapping(value = {"/", "/index"})
    public String indexGet(Model model) {
        //public String indexGet(@RequestParam(name="who", required=false) UserEntity userModel, Model model) {

        String auth = authenticatedName.getAuthenticatedName();
        if (auth != null) {
            UserEntity userModel = registrationDAO.getByUsername(auth);
            if (userModel == null) return "login";
            userDTO.setId(userModel.getId());
            userDTO.setUser(auth);
            userDTO.setPwd(userModel.getPassword());
            userDTO.setFirstname(userModel.getFirstname());
            userDTO.setLastname(userModel.getLastname());
            userDTO.setEmail(userModel.getEmail());
            model.addAttribute("who", userModel);
            if (userDTO.getId() != null && !userDTO.getId().equals("")) {
                seoSettingsDTO.entityToService(seoSettingsDAO.readById(userDTO.getId()));
            }
        }

        model.addAttribute("useraccountdetails", useraccountdetails);
        model.addAttribute("indexproperty", indexproperty);
        model.addAttribute("indexmanagement", indexmanagement);
        model.addAttribute("indexsystem", indexsystem);
        model.addAttribute("indexhelp1", indexhelp1);
        model.addAttribute("indexhelp2", indexhelp2);
        model.addAttribute("indexhelp3", indexhelp3);
        model.addAttribute("indexhelp4", indexhelp4);
        model.addAttribute("indexseo", indexseo);
        model.addAttribute("indexsecrets", indexsecrets);
        model.addAttribute("indeximprove", indeximprove);
        model.addAttribute("indexpractically", indexpractically);
        model.addAttribute("indexfulfill", indexfulfill);
        model.addAttribute("indexhelp12", indexhelp12);
        model.addAttribute("indexconsole", indexconsole);
        model.addAttribute("indexsearch", indexsearch);
        model.addAttribute("indexseo2", indexseo2);
        model.addAttribute("indextry", indextry);
        model.addAttribute("indexgives", indexgives);
        model.addAttribute("indexlogin", indexlogin);
        model.addAttribute("indexprovides", indexprovides);
        model.addAttribute("indexregister", indexregister);
        model.addAttribute("indexall", indexall);
        model.addAttribute("indexhome", indexhome);
        model.addAttribute("indexenter", indexenter);
        model.addAttribute("indexseosettings", indexseosettings);
        model.addAttribute("indexifyou", indexifyou);
        model.addAttribute("indexsaveimages", indexsaveimages);
        model.addAttribute("indexdrop", indexdrop);
        model.addAttribute("indexand", indexand);
        model.addAttribute("indexenjoy", indexenjoy);
        model.addAttribute("indexsearchandpublish", indexsearchandpublish);
        model.addAttribute("indexcontact", indexcontact);
        model.addAttribute("indexfeedback", indexfeedback);
        model.addAttribute("indexswing", indexswing);
        model.addAttribute("indexor", indexor);
        model.addAttribute("indexsend", indexsend);
        model.addAttribute("indextothetop", indextothetop);

        return "index";
    }

    /**
     * Get controller for /login path
     */
    @GetMapping(value = {"/login"})
    public String loginGet(Model model) {
        model.addAttribute("indexhelp5", indexhelp5);
        model.addAttribute("indexhelp6", indexhelp6);
        return "login";
    }
}