Search Engine Optimization

A Spring Boot and Thymeleaf project.

Improve the visibility of a website to increase the volume of its traffic in general, the number of its customers in particular. A service accessible via the web which allows the improvement of the referencing of a website by the multiplication of relevant lexical occurrences and the creation of pages allowing the implementation of these occurrences. In two acronyms, an SEO (Search Engine Optimization) SAAS (Software As A Service) application.

Practically, from a significant URL, that is to say related to the object of the site, and provided by the customer, the application follows this URL, extracts the links found, goes back these links, these pages, by successive requests, feeding its operation by the extraction of these new URLs, and creates web pages from the data collected and processed. Finally, the application puts these newly created pages online in the website to be promoted.

To fulfill this mission, the application mobilizes various software means, including a web crawler or web spider, software which automatically explores the web to collect resources and a web scraper or web harvester, software allowing content to be extracted from websites in the aim of transforming this material and allowing its use in another context, in this case referencing.

Made with Spring Boot and Thymeleaf
